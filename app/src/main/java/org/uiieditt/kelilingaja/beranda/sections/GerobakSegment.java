package org.uiieditt.kelilingaja.beranda.sections;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.beranda.sections.gerobak.GerobakAdapter;
import org.uiieditt.kelilingaja.lapak.LapakRetrofit;
import org.uiieditt.kelilingaja.util.UserPreferences;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLHandshakeException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GerobakSegment extends Fragment implements LocationListener, Callback<LapakNearby> {

    private static final String TAG = GerobakSegment.class.getSimpleName();

    private GerobakAdapter mAdapter;

    public LocationManager locationManager;

    private ProgressBar loadingSpin;
    private TextView empty;

    private boolean IsLogin = false;

    private Button loginButton;

    public GerobakSegment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static GerobakSegment newInstance(int sectionNumber) {
        GerobakSegment fragment = new GerobakSegment();
        Bundle args = new Bundle();
        args.putInt("1", sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        UserPreferences.initializeInstance(getContext());

        boolean token = (UserPreferences.getInstance().getValue("TOKEN") != null);
        boolean userId = (UserPreferences.getInstance().getValue("U_ID") != null);

        if (token && userId) {
            IsLogin = true;
        }

        if (IsLogin) {
            if (locationManager == null) {
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            }

            boolean fineLocGrant = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
            Log.v(TAG, String.valueOf(fineLocGrant));
            if (fineLocGrant) {
                // Create a very simple REST adapter which points the GitHub API.
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Apps.API_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        //.client(Apps.AbstractConnection())
                        .build();

                // Create an instance of our GitHub API interface.
                LapakRetrofit lapakRetrofit = retrofit.create(LapakRetrofit.class);

                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    // Create a call instance for looking up Retrofit contributors.
                    Call<LapakNearby> call = lapakRetrofit.ownerLapak(latitude, longitude, UserPreferences.getInstance().getValue("U_ID"));
                    call.enqueue(this);
                    Log.v(TAG, "LOCATION FOUND");
                } else {
                    Log.v(TAG, "LOCATION NULL");

                    //TODO: begin fix for searching best location found
                    List<String> providers = locationManager.getProviders(true);
                    Location bestLocation = null;
                    for (String provider : providers) {
                        Location l = locationManager.getLastKnownLocation(provider);
                        if (l == null) {
                            continue;
                        }
                        if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                            bestLocation = l;
                        }
                    }
                    if (bestLocation != null) {
                        double latitude = bestLocation.getLatitude();
                        double longitude = bestLocation.getLongitude();

                        // Create a call instance for looking up Retrofit contributors.
                        Call<LapakNearby> call = lapakRetrofit.ownerLapak(latitude, longitude, UserPreferences.getInstance().getValue("U_ID"));
                        call.enqueue(this);
                        Log.v(TAG, "LOCATION FOUND");
                    }
                    //TODO: end of fix for searching best location found
                }
            }
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.segment_gerobak, container, false);

        UserPreferences.initializeInstance(getContext());

        boolean token = (UserPreferences.getInstance().getValue("TOKEN") != null);
        boolean userId = (UserPreferences.getInstance().getValue("U_ID") != null);

        if (token && userId) {
            IsLogin = true;
        }

        loginButton = (Button) rootView.findViewById(R.id.login_button);
        empty = (TextView) rootView.findViewById(R.id.recomendation_recycler_empty);
        loadingSpin = (ProgressBar) rootView.findViewById(R.id.recomendation_recycler_loading);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recomendation_recycler_view);

        Log.v("LOGIN", String.valueOf(IsLogin));

        if (!IsLogin) {
            loadingSpin.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
            empty.setText(getString(R.string.app_text_login));
            loginButton.setVisibility(View.VISIBLE);
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(Apps.LOGIN_URL));
                    getActivity().finish();
                    startActivity(i);
                }
            });
        } else {
            mAdapter = new GerobakAdapter(new ArrayList<LapakNearby.Lapak>());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        UserPreferences.initializeInstance(getContext());

        if (IsLogin) {
            if (locationManager == null) {
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            }

            boolean fineLocGrant = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

            if (fineLocGrant) {
                // Create a very simple REST adapter which points the GitHub API.
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Apps.API_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(Apps.AbstractConnection())
                        .build();

                // Create an instance of our GitHub API interface.
                LapakRetrofit lapakRetrofit = retrofit.create(LapakRetrofit.class);

                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    // Create a call instance for looking up Retrofit contributors.
                    Call<LapakNearby> call = lapakRetrofit.ownerLapak(latitude, longitude, UserPreferences.getInstance().getValue("U_ID"));
                    call.enqueue(this);
                } else {
                    Log.v(TAG, "LOCATION NULL");

                    //TODO: begin fix for searching best location found
                    List<String> providers = locationManager.getProviders(true);
                    Location bestLocation = null;
                    for (String provider : providers) {
                        Location l = locationManager.getLastKnownLocation(provider);
                        if (l == null) {
                            continue;
                        }
                        if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                            bestLocation = l;
                        }
                    }
                    if (bestLocation != null) {
                        double latitude = bestLocation.getLatitude();
                        double longitude = bestLocation.getLongitude();

                        // Create a call instance for looking up Retrofit contributors.
                        Call<LapakNearby> call = lapakRetrofit.ownerLapak(latitude, longitude, UserPreferences.getInstance().getValue("U_ID"));
                        call.enqueue(this);
                        Log.v(TAG, "LOCATION FOUND");
                    }
                    //TODO: end of fix for searching best location found
                }

            }
        }

    }

    @Override
    public void onResponse(@NonNull Call<LapakNearby> call, @NonNull Response<LapakNearby> response) {
        Log.v(TAG, new Gson().toJson(response.body()));

        loadingSpin.setVisibility(View.GONE);
        LapakNearby.DataTemplate data = response.body().getData();
        reload(data.getLapak());

    }

    @Override
    public void onFailure(@NonNull Call<LapakNearby> call, @NonNull Throwable t) {

        Log.v(TAG, t.getMessage());

        loadingSpin.setVisibility(View.GONE);
        empty.setVisibility(View.VISIBLE);

        if (t instanceof UnknownHostException) {
            empty.setText(R.string.text_internet_unknown_host);
        }
        if (t instanceof SSLHandshakeException) {
            empty.setText(R.string.text_internet_ssl_problem);
        }
        if (t instanceof SocketTimeoutException) {
            empty.setText(R.string.text_internet_to_long);
        }
        if (t instanceof Exception) {
            empty.setText(R.string.text_internet_null);
        }

    }

    public void reload(@NonNull List<LapakNearby.Lapak> lapakData) {
        if (lapakData.size() == 0) {
            empty.setText(getString(R.string.lapak_anda_kosong));
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
            mAdapter.setMoviesList(lapakData);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.v(TAG, "onLocationChanged");

        locationManager.removeUpdates(this);

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        // Create a very simple REST adapter which points the GitHub API.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        LapakRetrofit lapakRetrofit = retrofit.create(LapakRetrofit.class);

        Call<LapakNearby> call = lapakRetrofit.ownerLapak(latitude, longitude, UserPreferences.getInstance().getValue("U_ID"));
        call.enqueue(this);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            switch (requestCode) {
                case 302:

                    boolean fineLocGrant = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

                    if (fineLocGrant) {
                        // Create a very simple REST adapter which points the GitHub API.
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(Apps.API_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .client(Apps.AbstractConnection())
                                .build();

                        // Create an instance of our GitHub API interface.
                        LapakRetrofit lapakRetrofit = retrofit.create(LapakRetrofit.class);

                        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            double latitude = location.getLatitude();
                            double longitude = location.getLongitude();

                            // Create a call instance for looking up Retrofit contributors.
                            Call<LapakNearby> call = lapakRetrofit.ownerLapak(latitude, longitude, UserPreferences.getInstance().getValue("U_ID"));
                            call.enqueue(this);
                        } else {
                            List<String> providers = locationManager.getProviders(true);
                            Location bestLocation = null;
                            for (String provider : providers) {
                                Location l = locationManager.getLastKnownLocation(provider);
                                if (l == null) {
                                    continue;
                                }
                                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                                    bestLocation = l;
                                }
                            }
                            if (bestLocation != null) {
                                double latitude = bestLocation.getLatitude();
                                double longitude = bestLocation.getLongitude();

                                // Create a call instance for looking up Retrofit contributors.
                                Call<LapakNearby> call = lapakRetrofit.ownerLapak(latitude, longitude, UserPreferences.getInstance().getValue("U_ID"));
                                call.enqueue(this);
                                Log.v(TAG, "LOCATION FOUND");
                            }
                        }

                    }

                    break;
            }
        }
    }
}