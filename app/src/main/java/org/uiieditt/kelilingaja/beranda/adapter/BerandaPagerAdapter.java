package org.uiieditt.kelilingaja.beranda.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.uiieditt.kelilingaja.beranda.BaseSegment;
import org.uiieditt.kelilingaja.beranda.MapSegment;

public class BerandaPagerAdapter extends FragmentPagerAdapter {

    public BerandaPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return MapSegment.newInstance(position);
            case 1:
                return BaseSegment.newInstance(position);
        }
        return BaseSegment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Explore";
            case 1:
                return "Recommendation";
        }
        return null;
    }
}