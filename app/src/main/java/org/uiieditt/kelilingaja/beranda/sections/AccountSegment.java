package org.uiieditt.kelilingaja.beranda.sections;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.KelilingajaActivity;
import org.uiieditt.kelilingaja.ProfileActivity;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.ApiUser;
import org.uiieditt.kelilingaja.beranda.sections.account.AccountRetrofit;
import org.uiieditt.kelilingaja.saldo.SaldoActivity;
import org.uiieditt.kelilingaja.util.UserPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AccountSegment extends Fragment implements Callback<ApiUser>, View.OnClickListener,
        DialogInterface.OnClickListener {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public TextView userNama;
    public TextView userEmail;
    public TextView userPhone;

    public Button editProfile;
    public Button logout;

    public RelativeLayout accountPlaceholder;

    public LinearLayout accountSaldo;

    public AccountSegment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AccountSegment newInstance(int sectionNumber) {
        AccountSegment fragment = new AccountSegment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        UserPreferences.initializeInstance(getActivity());

        String token = UserPreferences.getInstance().getValue("TOKEN");

        if (token != null) {
            View rootView = inflater.inflate(R.layout.segment_account, container, false);

            userNama = (TextView) rootView.findViewById(R.id.account_name);
            userEmail = (TextView) rootView.findViewById(R.id.account_email);
            userPhone = (TextView) rootView.findViewById(R.id.account_phone);

            editProfile = (Button) rootView.findViewById(R.id.profile_edit);
            logout = (Button) rootView.findViewById(R.id.profile_action_logout);

            accountSaldo = (LinearLayout) rootView.findViewById(R.id.account_linear_saldo);

            editProfile.setOnClickListener(this);
            accountSaldo.setOnClickListener(this);
            logout.setOnClickListener(this);

            accountPlaceholder = (RelativeLayout) rootView.findViewById(R.id.account_placeholder);
            accountPlaceholder.setVisibility(View.INVISIBLE);

            // Create a very simple REST adapter which points the GitHub API.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.CREDENTIAL_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Apps.AbstractConnection())
                    .build();

            // Create an instance of our GitHub API interface.
            AccountRetrofit apiUser = retrofit.create(AccountRetrofit.class);

            // Create a call instance for looking up Retrofit contributors.
            Call<ApiUser> call = apiUser.apiUser(token);
            call.enqueue(this);

            return rootView;

        } else {

            View rootView = inflater.inflate(R.layout.segment_account_empty, container, false);

            Button login = (Button) rootView.findViewById(R.id.account_login);
            login.setOnClickListener(loginButton);

            return rootView;
        }
    }

    View.OnClickListener loginButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //change to browser login
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(Apps.LOGIN_URL));
            getActivity().finish();
            startActivity(i);
        }
    };

    @Override
    public void onResponse(@NonNull Call<ApiUser> call, @NonNull Response<ApiUser> response) {

        ApiUser.DataTemplate data = response.body().getData();
        ApiUser.User u = data.getUser();

        userNama.setText(u.fullname);
        userEmail.setText(u.firstemail);
        userPhone.setText(u.phonenumber);
        accountPlaceholder.setVisibility(View.VISIBLE);

    }

    @Override
    public void onFailure(@NonNull Call<ApiUser> call, @NonNull Throwable t) {

        String fullname = UserPreferences.getInstance().getValue("U_NAMA");
        String firstemail = UserPreferences.getInstance().getValue("U_EMAIL");
        String phonenumber = UserPreferences.getInstance().getValue("U_PHONE");

        userNama.setText(fullname);
        userEmail.setText(firstemail);
        userPhone.setText(phonenumber);
        accountPlaceholder.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_edit:
                Intent edit = new Intent(view.getContext(), ProfileActivity.class);
                startActivity(edit);
                break;
            case R.id.account_linear_saldo:
                Intent intentSaldo = new Intent(view.getContext(), SaldoActivity.class);
                startActivity(intentSaldo);
                break;
            case R.id.profile_action_logout:
                new AlertDialog.Builder(getActivity())
                        .setIcon(R.drawable.ic_warning_black_48dp)
                        .setTitle(getString(R.string.logout_title))
                        .setMessage(getString(R.string.logout_desc))
                        .setPositiveButton(getString(R.string.ya), this)
                        .setNegativeButton(getString(R.string.tidak), this)
                        .show();
                break;
        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            UserPreferences.getInstance().clear();

            Intent add = new Intent(getActivity(), KelilingajaActivity.class);
            startActivity(add);

            getActivity().finish();
        }
    }
}