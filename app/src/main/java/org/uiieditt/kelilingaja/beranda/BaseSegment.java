package org.uiieditt.kelilingaja.beranda;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.uiieditt.kelilingaja.KelilingajaActivity;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.beranda.sections.adapter.SectionsPagerAdapter;

public class BaseSegment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private Toolbar toolbar;

    private int[] tabIcons = {
            R.drawable.ic_whatshot_white_48dp,
            R.drawable.ic_shopping_cart_white_48dp
    };

    public BaseSegment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static BaseSegment newInstance(int sectionNumber) {
        BaseSegment fragment = new BaseSegment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.segment_base, container, false);

        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((KelilingajaActivity) getActivity()).setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        /*
      The {@link android.support.v4.view.PagerAdapter} that will provide
      fragments for each of the sections. We use a
      {@link FragmentPagerAdapter} derivative, which will keep every
      loaded fragment in memory. If this becomes too memory intensive, it
      may be best to switch to a
      {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
        SectionsPagerAdapter fragmentSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        /*
      The {@link ViewPager} that will host the section contents.
     */
        ViewPager fragmentViewPager = (ViewPager) rootView.findViewById(R.id.fragment_container);
        fragmentViewPager.setAdapter(fragmentSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(fragmentViewPager);

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);

        fragmentViewPager.addOnPageChangeListener(pagerUtil);

        return rootView;
    }

    ViewPager.OnPageChangeListener pagerUtil = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (toolbar != null) {
                switch (position) {
                    case 0:
                        toolbar.setTitle(getString(R.string.app_name));
                        break;
                    case 1:
                        toolbar.setTitle(getString(R.string.lapak_anda));
                        break;
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}