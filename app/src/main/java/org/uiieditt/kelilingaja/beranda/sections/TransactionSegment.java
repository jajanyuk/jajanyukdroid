package org.uiieditt.kelilingaja.beranda.sections;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.beranda.sections.transaction.Transaction;
import org.uiieditt.kelilingaja.beranda.sections.transaction.TransactionAdapter;

import java.util.ArrayList;
import java.util.List;

public class TransactionSegment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private List<Transaction> movieList;
    private RecyclerView recyclerView;
    private TransactionAdapter mAdapter;

    public TransactionSegment() {
        movieList = new ArrayList<>();
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static TransactionSegment newInstance(int sectionNumber) {
        TransactionSegment fragment = new TransactionSegment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.segment_transaction, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.transaction_recycler_view);

        mAdapter = new TransactionAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareTransactionData();

        return rootView;
    }

    private void prepareTransactionData() {
        Transaction Transaction = new Transaction("Mad Max: Fury Road", "Action & Adventure", "2015");
        movieList.add(Transaction);

        Transaction = new Transaction("Inside Out", "Animation, Kids & Family", "2015");
        movieList.add(Transaction);

        Transaction = new Transaction("Star Wars: Episode VII - The Force Awakens", "Action", "2015");
        movieList.add(Transaction);

        Transaction = new Transaction("Shaun the Sheep", "Animation", "2015");
        movieList.add(Transaction);

        Transaction = new Transaction("The Martian", "Science Fiction & Fantasy", "2015");
        movieList.add(Transaction);

        Transaction = new Transaction("Mission: Impossible Rogue Nation", "Action", "2015");
        movieList.add(Transaction);

        Transaction = new Transaction("Up", "Animation", "2009");
        movieList.add(Transaction);

        Transaction = new Transaction("Star Trek", "Science Fiction", "2009");
        movieList.add(Transaction);

        Transaction = new Transaction("The LEGO Transaction", "Animation", "2014");
        movieList.add(Transaction);

        Transaction = new Transaction("Iron Man", "Action & Adventure", "2008");
        movieList.add(Transaction);

        Transaction = new Transaction("Aliens", "Science Fiction", "1986");
        movieList.add(Transaction);

        Transaction = new Transaction("Chicken Run", "Animation", "2000");
        movieList.add(Transaction);

        Transaction = new Transaction("Back to the Future", "Science Fiction", "1985");
        movieList.add(Transaction);

        Transaction = new Transaction("Raiders of the Lost Ark", "Action & Adventure", "1981");
        movieList.add(Transaction);

        Transaction = new Transaction("Goldfinger", "Action & Adventure", "1965");
        movieList.add(Transaction);

        Transaction = new Transaction("Guardians of the Galaxy", "Science Fiction & Fantasy", "2014");
        movieList.add(Transaction);

        mAdapter.notifyDataSetChanged();
    }
}