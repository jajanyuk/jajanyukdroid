package org.uiieditt.kelilingaja.beranda.sections;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.beranda.sections.recomendation.RecommendationAdapter;
import org.uiieditt.kelilingaja.beranda.sections.recomendation.RecommendationRetrofit;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLHandshakeException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecomendationSegment extends Fragment implements LocationListener, Callback<LapakNearby> {

    private static final String TAG = RecomendationSegment.class.getSimpleName();

    private RecommendationAdapter mAdapter;

    public LocationManager locationManager;

    private ProgressBar loadingSpin;
    private TextView empty;

    public RecomendationSegment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static RecomendationSegment newInstance(int sectionNumber) {
        RecomendationSegment fragment = new RecomendationSegment();
        Bundle args = new Bundle();
        args.putInt("1", sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (locationManager == null) {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        }

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        Log.v(TAG, String.valueOf(fineLocGrant));
        if (fineLocGrant) {
            // Create a very simple REST adapter which points the GitHub API.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    //.client(Apps.AbstractConnection())
                    .build();

            // Create an instance of our GitHub API interface.
            RecommendationRetrofit nearbyAdapter = retrofit.create(RecommendationRetrofit.class);

            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                // Create a call instance for looking up Retrofit contributors.
                Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
                call.enqueue(this);
                Log.v(TAG, "LOCATION FOUND");
            } else {
                Log.v(TAG, "LOCATION NULL");

                //TODO: begin fix for searching best location found
                List<String> providers = locationManager.getProviders(true);
                Location bestLocation = null;
                for (String provider : providers) {
                    Location l = locationManager.getLastKnownLocation(provider);
                    if (l == null) {
                        continue;
                    }
                    if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                        bestLocation = l;
                    }
                }
                if (bestLocation != null) {
                    double latitude = bestLocation.getLatitude();
                    double longitude = bestLocation.getLongitude();

                    // Create a call instance for looking up Retrofit contributors.
                    Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
                    call.enqueue(this);
                    Log.v(TAG, "LOCATION FOUND");
                }
                //TODO: end of fix for searching best location found
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.segment_recomendation, container, false);

        empty = (TextView) rootView.findViewById(R.id.recomendation_recycler_empty);
        loadingSpin = (ProgressBar) rootView.findViewById(R.id.recomendation_recycler_loading);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recomendation_recycler_view);

        mAdapter = new RecommendationAdapter(new ArrayList<LapakNearby.Lapak>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (locationManager == null) {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        }

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {
            // Create a very simple REST adapter which points the GitHub API.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Apps.AbstractConnection())
                    .build();

            // Create an instance of our GitHub API interface.
            RecommendationRetrofit nearbyAdapter = retrofit.create(RecommendationRetrofit.class);

            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                // Create a call instance for looking up Retrofit contributors.
                Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
                call.enqueue(this);
            } else {
                Log.v(TAG, "LOCATION NULL");

                //TODO: begin fix for searching best location found
                List<String> providers = locationManager.getProviders(true);
                Location bestLocation = null;
                for (String provider : providers) {
                    Location l = locationManager.getLastKnownLocation(provider);
                    if (l == null) {
                        continue;
                    }
                    if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                        bestLocation = l;
                    }
                }
                if (bestLocation != null) {
                    double latitude = bestLocation.getLatitude();
                    double longitude = bestLocation.getLongitude();

                    // Create a call instance for looking up Retrofit contributors.
                    Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
                    call.enqueue(this);
                    Log.v(TAG, "LOCATION FOUND");
                }
                //TODO: end of fix for searching best location found
            }

        } else {
            //feature not allowed
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, 1);
        }
    }

    @Override
    public void onResponse(@NonNull Call<LapakNearby> call, @NonNull Response<LapakNearby> response) {
        //Log.v(TAG, new Gson().toJson(response.body()));
        if (response.body().getData() != null) {
            loadingSpin.setVisibility(View.GONE);
            LapakNearby.DataTemplate data = response.body().getData();
            reload(data.getLapak());
        }

    }

    @Override
    public void onFailure(@NonNull Call<LapakNearby> call, @NonNull Throwable t) {

        Log.v(TAG, t.getMessage());

        loadingSpin.setVisibility(View.GONE);
        empty.setVisibility(View.VISIBLE);

        if (t instanceof UnknownHostException) {
            empty.setText(R.string.text_internet_unknown_host);
        }
        if (t instanceof SSLHandshakeException) {
            empty.setText(R.string.text_internet_ssl_problem);
        }
        if (t instanceof SocketTimeoutException) {
            empty.setText(R.string.text_internet_to_long);
        }
        if (t instanceof Exception) {
            empty.setText(R.string.text_internet_null);
        }

    }

    public void reload(@NonNull List<LapakNearby.Lapak> lapakData) {
        if (lapakData.size() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
            mAdapter.setMoviesList(lapakData);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //Hey, a non null location! Sweet!
        Log.v(TAG, "onLocationChanged");
        //remove location callback:
        locationManager.removeUpdates(this);

        //open the map:
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        // Create a very simple REST adapter which points the GitHub API.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        // Create an instance of our GitHub API interface.
        RecommendationRetrofit nearbyAdapter = retrofit.create(RecommendationRetrofit.class);
        // Create a call instance for looking up Retrofit contributors.
        Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
        call.enqueue(this);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {
            // Create a very simple REST adapter which points the GitHub API.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Apps.AbstractConnection())
                    .build();

            // Create an instance of our GitHub API interface.
            RecommendationRetrofit nearbyAdapter = retrofit.create(RecommendationRetrofit.class);

            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                // Create a call instance for looking up Retrofit contributors.
                Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
                call.enqueue(this);
            } else {
                List<String> providers = locationManager.getProviders(true);
                Location bestLocation = null;
                for (String provider : providers) {
                    Location l = locationManager.getLastKnownLocation(provider);
                    if (l == null) {
                        continue;
                    }
                    if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                        bestLocation = l;
                    }
                }
                if (bestLocation != null) {
                    double latitude = bestLocation.getLatitude();
                    double longitude = bestLocation.getLongitude();

                    // Create a call instance for looking up Retrofit contributors.
                    Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
                    call.enqueue(this);
                    Log.v(TAG, "LOCATION FOUND");
                }
            }
        }
    }
}