package org.uiieditt.kelilingaja.beranda.sections.recomendation;

import org.uiieditt.kelilingaja.api.data.LapakNearby;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RecommendationRetrofit {

    @GET("lapak/nearby/{lat}/{lng}")
    Call<LapakNearby> nearby(@Path("lat") Double latitude, @Path("lng") Double longitude);

}
