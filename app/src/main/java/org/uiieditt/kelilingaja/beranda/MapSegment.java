package org.uiieditt.kelilingaja.beranda;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.beranda.sections.recomendation.RecommendationRetrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapSegment extends Fragment implements LocationListener, OnMapReadyCallback, Callback<LapakNearby> {

    private static final String TAG = MapSegment.class.getSimpleName();

    public LocationManager locationManager;

    private MapView mapView;
    private GoogleMap mMap;

    public MapSegment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MapSegment newInstance(int sectionNumber) {
        MapSegment fragment = new MapSegment();
        Bundle args = new Bundle();
        args.putInt("0", sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.segment_map, container, false);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mapView = (MapView) rootView.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        // Gets to GoogleMap from the MapView and does initialization stuff
        mapView.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map_json));
        mMap = googleMap;

        //hardcode to bandung
        double latitudeDefgault = -6.914744;
        double longitudeDefault = 107.609810;

        LatLng latLngDefault = new LatLng(latitudeDefgault, longitudeDefault);
        CameraUpdate cameraUpdateDefault = CameraUpdateFactory.newLatLngZoom(latLngDefault, 12);
        mMap.animateCamera(cameraUpdateDefault);

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {

            // Create a very simple REST adapter which points the GitHub API.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Apps.AbstractConnection())
                    .build();

            // Create an instance of our GitHub API interface.
            RecommendationRetrofit nearbyAdapter = retrofit.create(RecommendationRetrofit.class);

            mMap.setMyLocationEnabled(true);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                LatLng latLng = new LatLng(latitude, longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                mMap.animateCamera(cameraUpdate);

                // Create a call instance for looking up Retrofit contributors.
                Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
                call.enqueue(this);

            } else {
                //TODO: begin fix for searching best location found
                List<String> providers = locationManager.getProviders(true);
                Location bestLocation = null;
                for (String provider : providers) {
                    Location l = locationManager.getLastKnownLocation(provider);
                    if (l == null) {
                        continue;
                    }
                    if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                        bestLocation = l;
                    }
                }
                if (bestLocation != null) {
                    double latitude = bestLocation.getLatitude();
                    double longitude = bestLocation.getLongitude();

                    LatLng latLng = new LatLng(latitude, longitude);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                    mMap.animateCamera(cameraUpdate);

                    // Create a call instance for looking up Retrofit contributors.
                    Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
                    call.enqueue(this);
                    Log.v(TAG, "LOCATION FOUND");
                }
                //TODO: end of fix for searching best location found
            }
        }

        //bug resume it for load maps
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onResponse(@NonNull Call<LapakNearby> call, @NonNull Response<LapakNearby> response) {
        if (response.body().getData() != null) {
            LapakNearby.DataTemplate data = response.body().getData();
            if (mMap != null) {
                for (LapakNearby.Lapak lapak : data.getLapak()) {
                    LatLng here = new LatLng(Double.valueOf(lapak.getLat()), Double.valueOf(lapak.getLng()));
                    mMap.addMarker(new MarkerOptions().position(here).title(lapak.getNamalapak()));
                }
            }
        }

    }

    @Override
    public void onFailure(@NonNull Call<LapakNearby> call, @NonNull Throwable t) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Hey, a non null location! Sweet!
        //remove location callback:
        locationManager.removeUpdates(this);

        //open the map:
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        LatLng latLng = new LatLng(latitude, longitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        if (mMap != null) {
            mMap.animateCamera(cameraUpdate);
        }

        // Create a very simple REST adapter which points the GitHub API.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        // Create an instance of our GitHub API interface.
        RecommendationRetrofit nearbyAdapter = retrofit.create(RecommendationRetrofit.class);
        // Create a call instance for looking up Retrofit contributors.
        Call<LapakNearby> call = nearbyAdapter.nearby(latitude, longitude);
        call.enqueue(this);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(this);
    }
}
