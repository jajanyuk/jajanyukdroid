package org.uiieditt.kelilingaja.beranda.sections.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.uiieditt.kelilingaja.beranda.sections.AccountSegment;
import org.uiieditt.kelilingaja.beranda.sections.GerobakSegment;
import org.uiieditt.kelilingaja.beranda.sections.RecomendationSegment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return RecomendationSegment.newInstance(position);
            case 1:
                return GerobakSegment.newInstance(position);
        }
        return AccountSegment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}