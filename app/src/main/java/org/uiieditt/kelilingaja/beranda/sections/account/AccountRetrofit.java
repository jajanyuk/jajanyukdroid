package org.uiieditt.kelilingaja.beranda.sections.account;

import org.uiieditt.kelilingaja.api.data.ApiUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AccountRetrofit {

    @FormUrlEncoded
    @POST("api/user")
    Call<ApiUser> apiUser(@Field("token") String token);

    @FormUrlEncoded
    @POST("api/user/edit")
    Call<ApiUser> apiUserEdit(@Field("token") String token, @Field("nama") String nama,
                              @Field("email") String email, @Field("telp") String telp);

}
