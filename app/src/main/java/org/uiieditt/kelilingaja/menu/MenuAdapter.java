package org.uiieditt.kelilingaja.menu;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.MenuLapak;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {

    private List<MenuLapak.Menu> menuList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_menu, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MenuLapak.Menu menu = menuList.get(position);

        holder.menu = menu;

        holder.name.setText(menu.getNama());
        holder.harga.setText(menu.getHarga());
        holder.terjual.setText(menu.getDeskripsi());

        Context context = holder.image.getContext();

        Picasso.with(context).load(menu.getImageurl()).fit().centerCrop().into(holder.image);
    }

    public MenuAdapter(List<MenuLapak.Menu> menuList) {
        this.menuList = menuList;
    }

    public void setMenuList(List<MenuLapak.Menu> menuList) {
        this.menuList = menuList;
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,
            View.OnClickListener, NumberPicker.OnValueChangeListener {

        public TextView name, harga, terjual;

        public ImageView image;

        public MenuLapak.Menu menu;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.list_row_menu_name);
            harga = (TextView) view.findViewById(R.id.list_row_menu_harga);
            terjual = (TextView) view.findViewById(R.id.list_row_menu_terjual);

            image = (ImageView) view.findViewById(R.id.list_row_menu_image);

            //view.setOnCreateContextMenuListener(this);
            //view.setOnClickListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0, v.getId(), 0, "Call");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "SMS");
        }

        @Override
        public void onClick(View v) {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(v.getContext());
            View promptsView = li.inflate(R.layout.dialog_pesan_menu, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());

            // set prompts.xml to alertdialog builder
            alertDialogBuilder.setView(promptsView);

            final NumberPicker np = (NumberPicker) promptsView.findViewById(R.id.numberPicker);
            np.setMaxValue(10);
            np.setMinValue(1);
            np.setWrapSelectorWheel(false);
            np.setOnValueChangedListener(this);

            // set dialog message
            alertDialogBuilder
                    .setCancelable(false)
                    .setTitle(menu.getNama())
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // get user input and set it to result
                                    // edit text
                                    // result.setText(userInput.getText());
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

        }

        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        }
    }
}
