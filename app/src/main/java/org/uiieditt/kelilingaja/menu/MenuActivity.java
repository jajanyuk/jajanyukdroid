package org.uiieditt.kelilingaja.menu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.api.data.MenuLapak;
import org.uiieditt.kelilingaja.util.UserPreferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MenuActivity extends AppCompatActivity implements Callback<MenuLapak> {

    private MenuAdapter mAdapter;

    private ProgressBar loading;
    private TextView empty;

    private String token;
    private String userid;

    @Override
    protected void onStart() {
        super.onStart();
        UserPreferences.initializeInstance(this);
        token = UserPreferences.getInstance().getValue("TOKEN");
        userid = UserPreferences.getInstance().getValue("U_ID");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Bundle data = getIntent().getExtras();
        LapakNearby.Lapak lapakObject = data.getParcelable("LAPAK");

        loading = (ProgressBar) findViewById(R.id.menu_recycler_loading);
        empty = (TextView) findViewById(R.id.menu_recycler_empty);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(lapakObject.getNamalapak());

        ImageView imageParalax = (ImageView) findViewById(R.id.colapsing_image);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle(lapakObject.getNamalapak());

        setSupportActionBar(toolbar);

        Picasso.with(this).load(lapakObject.getImageurl()).fit().centerCrop().into(imageParalax);

        //menuEmpty = (TextView) findViewById(R.id.menu_recycler_empty);
        RecyclerView menuRecyclerView = (RecyclerView) findViewById(R.id.menu_recycler_view);

        mAdapter = new MenuAdapter(new ArrayList<MenuLapak.Menu>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        menuRecyclerView.setLayoutManager(mLayoutManager);
        menuRecyclerView.setItemAnimator(new DefaultItemAnimator());
        menuRecyclerView.setAdapter(mAdapter);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        MenuRetrofit nearbyAdapter = retrofit.create(MenuRetrofit.class);

        Call<MenuLapak> call = nearbyAdapter.menuLapak(Integer.parseInt(lapakObject.getLapakid()));
        call.enqueue(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onResponse(@NonNull Call<MenuLapak> call, @NonNull Response<MenuLapak> response) {
        loading.setVisibility(View.GONE);
        MenuLapak.DataTemplate data = response.body().getData();
        reload(data.getMenu());
    }

    @Override
    public void onFailure(@NonNull Call<MenuLapak> call, @NonNull Throwable t) {
        loading.setVisibility(View.GONE);
        empty.setVisibility(View.VISIBLE);
    }

    public void reload(List<MenuLapak.Menu> lapakData) {
        if (lapakData.size() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
            mAdapter.setMenuList(lapakData);
            mAdapter.notifyDataSetChanged();
        }
    }
}
