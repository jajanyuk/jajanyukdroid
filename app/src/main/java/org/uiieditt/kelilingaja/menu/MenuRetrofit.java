package org.uiieditt.kelilingaja.menu;

import org.uiieditt.kelilingaja.api.data.MenuLapak;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MenuRetrofit {

    @GET("menu/lapak/{lapakid}")
    Call<MenuLapak> menuLapak(@Path("lapakid") int lapakid);

}
