package org.uiieditt.kelilingaja;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.uiieditt.kelilingaja.api.data.ApiUser;
import org.uiieditt.kelilingaja.beranda.sections.account.AccountRetrofit;
import org.uiieditt.kelilingaja.util.UserPreferences;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLHandshakeException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileActivity extends AppCompatActivity implements Callback<ApiUser>, View.OnClickListener {

    private static final String TAG = ProfileActivity.class.getSimpleName();

    public Button editProfile;

    public EditText nama;
    public EditText email;
    public EditText telpon;

    private ProgressDialog progress;

    private DialogInterface.OnClickListener alertOk = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            UserPreferences.getInstance().clear();

            Intent add = new Intent(ProfileActivity.this, KelilingajaActivity.class);
            startActivity(add);
            finish();
        }
    };

    private DialogInterface.OnClickListener alertCancel = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        nama = (EditText) findViewById(R.id.profile_edit_nama);
        email = (EditText) findViewById(R.id.profile_edit_email);
        telpon = (EditText) findViewById(R.id.profile_edit_telpon);

        editProfile = (Button) findViewById(R.id.profile_edit_simpan);
        editProfile.setOnClickListener(this);

        String token = UserPreferences.getInstance().getValue("TOKEN");
        if (token != null) {

            progress = ProgressDialog.show(this, "Mengambil Data", "Mengambil data profil anda dari server", true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.CREDENTIAL_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            AccountRetrofit apiUser = retrofit.create(AccountRetrofit.class);

            Call<ApiUser> call = apiUser.apiUser(token);
            call.enqueue(new Callback<ApiUser>() {

                @Override
                public void onResponse(@NonNull Call<ApiUser> call, @NonNull Response<ApiUser> response) {

                    progress.dismiss();

                    ApiUser.DataTemplate data = response.body().getData();
                    ApiUser.User u = data.getUser();

                    nama.setText(u.fullname);
                    email.setText(u.firstemail);
                    telpon.setText(u.phonenumber);
                }

                @Override
                public void onFailure(@NonNull Call<ApiUser> call, @NonNull Throwable t) {

                    progress.dismiss();

                    Log.v(TAG, t.getMessage());

                    if (t instanceof UnknownHostException) {
                        FailureDialog(getString(R.string.text_internet_unknown_host));
                    }
                    if (t instanceof SSLHandshakeException) {
                        FailureDialog(getString(R.string.text_internet_ssl_problem));
                    }
                    if (t instanceof SocketTimeoutException) {
                        FailureDialog(getString(R.string.text_internet_to_long));
                    }
                    if (t instanceof Exception) {
                        FailureDialog(getString(R.string.text_internet_null));
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user, menu);

        /*
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        */

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.menu_user_logout) {
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.ic_warning_black_48dp)
                    .setTitle(getString(R.string.logout_title))
                    .setMessage(getString(R.string.logout_desc))
                    .setPositiveButton(getString(R.string.ya), alertOk)
                    .setNegativeButton(getString(R.string.tidak), alertCancel)
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(@NonNull Call<ApiUser> call, @NonNull Response<ApiUser> response) {

        progress.dismiss();

        ApiUser.DataTemplate data = response.body().getData();
        ApiUser.User u = data.getUser();

        nama.setText(u.fullname);
        email.setText(u.firstemail);
        telpon.setText(u.phonenumber);

        Toast.makeText(this, getString(R.string.berhasil_simpan), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(@NonNull Call<ApiUser> call, @NonNull Throwable t) {

        progress.dismiss();

        Log.v(TAG, t.getMessage());

        if (t instanceof UnknownHostException) {
            FailureDialog(getString(R.string.text_internet_unknown_host));
        }
        if (t instanceof SSLHandshakeException) {
            FailureDialog(getString(R.string.text_internet_ssl_problem));
        }
        if (t instanceof SocketTimeoutException) {
            FailureDialog(getString(R.string.text_internet_to_long));
        }
        if (t instanceof Exception) {
            FailureDialog(getString(R.string.text_internet_null));
        }
    }

    @Override
    public void onClick(View view) {

        progress = ProgressDialog.show(this, "Menyimpan Data", "Harap tunggu proses penyimpanan data", true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.CREDENTIAL_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        AccountRetrofit apiUser = retrofit.create(AccountRetrofit.class);

        String token = UserPreferences.getInstance().getValue("TOKEN");
        Call<ApiUser> call = apiUser.apiUserEdit(token, nama.getText().toString(), email.getText().toString(), telpon.getText().toString());
        call.enqueue(this);
    }

    public void FailureDialog(String message) {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_black_48dp)
                .setTitle(getString(R.string.gagal))
                .setMessage(message)
                .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
