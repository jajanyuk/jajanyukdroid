package org.uiieditt.kelilingaja.job;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.support.PersistableBundleCompat;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.api.data.ApiUser;
import org.uiieditt.kelilingaja.service.UserServiceRetrofit;
import org.uiieditt.kelilingaja.util.UserPreferences;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LocationSyncJob extends Job {

    public static final String TAG = "LocationSyncJob";

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        // run your job here
        UserPreferences.initializeInstance(getContext());
        String token = UserPreferences.getInstance().getValue("FCM_REGID");
        String userid = UserPreferences.getInstance().getValue("U_ID");

        if (token == null && userid == null) {
            return Result.RESCHEDULE;
        }

        // Create a very simple REST adapter which points the GitHub API.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        // Create an instance of our GitHub API interface.
        UserServiceRetrofit fcmObj = retrofit.create(UserServiceRetrofit.class);

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {
            LocationManager lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                Call<ApiUser> call = fcmObj.apiUser(token, userid, location.getLatitude(), location.getLongitude());
                call.enqueue(userCallback);
                return Result.SUCCESS;
            } else {
                return Result.RESCHEDULE;
            }
        }
        return Result.FAILURE;
    }

    private static void scheduleAdvancedJob() {
        PersistableBundleCompat extras = new PersistableBundleCompat();
        extras.putString("key", "Hello world");

        int jobId = new JobRequest.Builder(LocationSyncJob.TAG)
                .setExecutionWindow(30_000L, 40_000L)
                .setBackoffCriteria(5_000L, JobRequest.BackoffPolicy.EXPONENTIAL)
                .setRequiresCharging(true)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setExtras(extras)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public static void schedulePeriodicJob() {
        int jobId = new JobRequest.Builder(LocationSyncJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))
                .setPersisted(true)
                .build()
                .schedule();
    }

    public static void scheduleExactJob() {
        int jobId = new JobRequest.Builder(LocationSyncJob.TAG)
                .setExact(20_000L)
                .setPersisted(true)
                .build()
                .schedule();
    }

    public static void cancelJob(int jobId) {
        JobManager.instance().cancel(jobId);
    }

    @Override
    protected void onReschedule(int newJobId) {
        // the rescheduled job has a new ID
    }

    private Callback<ApiUser> userCallback = new Callback<ApiUser>() {
        @Override
        public void onResponse(@NonNull Call<ApiUser> call, @NonNull Response<ApiUser> response) {
            Log.v(TAG, "userCallback RESPONSE SUCCESS");
        }

        @Override
        public void onFailure(@NonNull Call<ApiUser> call, @NonNull Throwable t) {
            Log.v(TAG, "userCallback RESPONSE FAILED");
        }
    };
}
