package org.uiieditt.kelilingaja.job;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class JajanyukJobCreator implements JobCreator {

    @Override
    public Job create(String tag) {
        switch (tag) {
            case LocationSyncJob.TAG:
                return new LocationSyncJob();
            default:
                return null;
        }
    }
}
