package org.uiieditt.kelilingaja;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import org.uiieditt.kelilingaja.beranda.adapter.BerandaPagerAdapter;
import org.uiieditt.kelilingaja.lapak.BukaLapakActivity;
import org.uiieditt.kelilingaja.support.AboutActivity;
import org.uiieditt.kelilingaja.support.ContactJajanyukActivity;
import org.uiieditt.kelilingaja.support.FaqActivity;
import org.uiieditt.kelilingaja.support.TermServiceActivity;
import org.uiieditt.kelilingaja.util.NotificationUtils;
import org.uiieditt.kelilingaja.util.UserPreferences;

public class KelilingajaActivity extends AppCompatActivity {

    private static final String TAG = KelilingajaActivity.class.getSimpleName();

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private BerandaPagerAdapter mBerandaPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public LocationManager locationManager;

    private String token;
    private String userid;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onStart() {
        super.onStart();
        UserPreferences.initializeInstance(this);
        token = UserPreferences.getInstance().getValue("TOKEN");
        userid = UserPreferences.getInstance().getValue("U_ID");

        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jajanyuk);

        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!fineLocGrant) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            boolean settingsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!settingsEnabled) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false);
                builder.setIcon(R.drawable.ic_settings_black_48dp);
                builder.setTitle(getString(R.string.need_gps_title));
                builder.setMessage(getString(R.string.need_gps_desc));
                builder.setPositiveButton(getString(R.string.need_gps_enable), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 302);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                // Create the adapter that will return a fragment for each of the three
                // primary sections of the activity.
                mBerandaPagerAdapter = new BerandaPagerAdapter(getSupportFragmentManager());

                // Set up the ViewPager with the sections adapter.
                mViewPager = (ViewPager) findViewById(R.id.activity_container);
                mViewPager.setAdapter(mBerandaPagerAdapter);

                mViewPager.setCurrentItem(1);

                mRegistrationBroadcastReceiver = new BroadcastReceiver() {

                    @Override
                    public void onReceive(Context context, Intent intent) {

                        // checking for type intent filter
                        if (intent.getAction().equals(Apps.REGISTRATION_COMPLETE)) {
                            // gcm successfully registered
                            // now subscribe to `global` topic to receive app wide notifications
                            FirebaseMessaging.getInstance().subscribeToTopic(Apps.TOPIC_GLOBAL);
                            displayFirebaseRegId();

                        } else if (intent.getAction().equals(Apps.PUSH_NOTIFICATION)) {
                            // new push notification is received
                            String message = intent.getStringExtra("message");
                            Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                        }
                    }
                };
            }
        }

        Log.v("POST", "ON CREATE JAJANYUK ACTIVITY");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Apps.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Apps.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

        Log.v("POST", "ON RESUME JAJANYUK ACTIVITY");
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        Log.v("POST", "ON RESUME FRAGMENT JAJANYUK ACTIVITY");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (token != null && userid != null) {
            getMenuInflater().inflate(R.menu.menu_application, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_application_global, menu);
        }

        /*
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        */

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (token != null && userid != null) {
            switch (item.getItemId()) {
                case R.id.menu_profil_user:
                    Intent user = new Intent(this, ProfileActivity.class);
                    startActivity(user);
                    break;
                case R.id.menu_application_terms_service:
                    Intent terms = new Intent(this, TermServiceActivity.class);
                    startActivity(terms);
                    break;
                case R.id.menu_application_contact_us:
                    Intent contact = new Intent(this, ContactJajanyukActivity.class);
                    startActivity(contact);
                    break;
                case R.id.menu_application_lapak:
                    Intent lapak = new Intent(this, BukaLapakActivity.class);
                    startActivity(lapak);
                    break;
                case R.id.menu_application_help:
                    Intent help = new Intent(this, FaqActivity.class);
                    startActivity(help);
                    break;
                case R.id.menu_application_about_application:
                    Intent about = new Intent(this, AboutActivity.class);
                    startActivity(about);
                    break;
            }
        } else {
            switch (item.getItemId()) {
                case R.id.menu_application_terms_service:
                    Intent terms = new Intent(this, TermServiceActivity.class);
                    startActivity(terms);
                    break;
                case R.id.menu_application_contact_us:
                    Intent contact = new Intent(this, ContactJajanyukActivity.class);
                    startActivity(contact);
                    break;
                case R.id.menu_application_help:
                    Intent help = new Intent(this, FaqActivity.class);
                    startActivity(help);
                    break;
                case R.id.menu_application_about_application:
                    Intent about = new Intent(this, AboutActivity.class);
                    startActivity(about);
                    break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mViewPager != null) {
            if (mViewPager.getCurrentItem() == 0) {
                mViewPager.setCurrentItem(1);
                return;
            }
        }

        super.onBackPressed();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Apps.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        if (!TextUtils.isEmpty(regId)) {
            Log.e("FCM", "Firebase Reg Id: " + regId);
        } else {
            Log.e("FCM", "Firebase Reg Id is not received yet!");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.v(TAG, "onRequestPermissionsResult");

        boolean settingsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!settingsEnabled) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setIcon(R.drawable.ic_settings_black_48dp);
            builder.setTitle(getString(R.string.need_gps_title));
            builder.setMessage(getString(R.string.need_gps_desc));
            builder.setPositiveButton(getString(R.string.need_gps_enable), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 302);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.
            mBerandaPagerAdapter = new BerandaPagerAdapter(getSupportFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) findViewById(R.id.activity_container);
            mViewPager.setAdapter(mBerandaPagerAdapter);

            mViewPager.setCurrentItem(1);

            mRegistrationBroadcastReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    // checking for type intent filter
                    if (intent.getAction().equals(Apps.REGISTRATION_COMPLETE)) {
                        // gcm successfully registered
                        // now subscribe to `global` topic to receive app wide notifications
                        FirebaseMessaging.getInstance().subscribeToTopic(Apps.TOPIC_GLOBAL);
                        displayFirebaseRegId();

                    } else if (intent.getAction().equals(Apps.PUSH_NOTIFICATION)) {
                        // new push notification is received
                        String message = intent.getStringExtra("message");
                        Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                    }
                }
            };
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v(TAG, "MAIN onActivityResult");

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mBerandaPagerAdapter = new BerandaPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.activity_container);
        mViewPager.setAdapter(mBerandaPagerAdapter);

        mViewPager.setCurrentItem(1);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Apps.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Apps.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Apps.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
    }
}
