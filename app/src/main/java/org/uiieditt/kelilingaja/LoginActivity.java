package org.uiieditt.kelilingaja;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.uiieditt.kelilingaja.api.data.ApiUser;
import org.uiieditt.kelilingaja.beranda.sections.account.AccountRetrofit;
import org.uiieditt.kelilingaja.job.LocationSyncJob;
import org.uiieditt.kelilingaja.service.UserServiceRetrofit;
import org.uiieditt.kelilingaja.util.UserPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements Callback<ApiUser>,
        DialogInterface.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private Callback<ApiUser> userCallback = new Callback<ApiUser>() {
        @Override
        public void onResponse(@NonNull Call<ApiUser> call, @NonNull Response<ApiUser> response) {
            startActivity(new Intent(getBaseContext(), KelilingajaActivity.class));
            finish();
        }

        @Override
        public void onFailure(@NonNull Call<ApiUser> call, @NonNull Throwable t) {
            Log.v(TAG, "userCallback RESPONSE FAILED");
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        UserPreferences.initializeInstance(this);
        Intent intent = getIntent();

        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();

            String token = uri.getQueryParameter("token");

            UserPreferences.initializeInstance(this);
            UserPreferences.getInstance().setValue("TOKEN", uri.getQueryParameter("token"));
            UserPreferences.getInstance().setValue("SECURE", uri.getQueryParameter("secure"));
            UserPreferences.getInstance().setValue("APP", uri.getQueryParameter("app"));

            // Create a very simple REST adapter which points the GitHub API.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.CREDENTIAL_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Apps.AbstractConnection())
                    .build();

            // Create an instance of our GitHub API interface.
            AccountRetrofit apiUser = retrofit.create(AccountRetrofit.class);

            // Create a call instance for looking up Retrofit contributors.
            Call<ApiUser> call = apiUser.apiUser(token);
            call.enqueue(this);
        }
    }

    @Override
    public void onResponse(@NonNull Call<ApiUser> call, @NonNull Response<ApiUser> response) {
        ApiUser.DataTemplate data = response.body().getData();
        ApiUser.User u = data.getUser();

        UserPreferences.getInstance().setValue("U_ID", u.getId());
        UserPreferences.getInstance().setValue("U_NAMA", u.getFullname());
        UserPreferences.getInstance().setValue("U_EMAIL", u.getFirstemail());
        UserPreferences.getInstance().setValue("U_PHONE", u.getPhonenumber());
        UserPreferences.getInstance().setValue("U_ALIAS", u.getAlias());

        sendRegistrationToServer(UserPreferences.getInstance().getValue("U_ID"));
        LocationSyncJob.schedulePeriodicJob();

        startActivity(new Intent(this, KelilingajaActivity.class));
        finish();
    }

    @Override
    public void onFailure(@NonNull Call<ApiUser> call, @NonNull Throwable t) {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_black_48dp)
                .setTitle("Login Gagal")
                .setMessage("Koneksi ke server login gagal. Ingin mencoba lagi.?")
                .setPositiveButton("Ya", this)
                .setNegativeButton("Yidak", this)
                .show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(Apps.LOGIN_URL));
            startActivity(i);
        } else {
            startActivity(new Intent(this, KelilingajaActivity.class));
        }
        finish();
    }

    private void sendRegistrationToServer(String userid) {
        UserPreferences.initializeInstance(getApplicationContext());
        String token = UserPreferences.getInstance().getValue("FCM_REGID");

        // Create a very simple REST adapter which points the GitHub API.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our GitHub API interface.
        UserServiceRetrofit fcmObj = retrofit.create(UserServiceRetrofit.class);

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {
            LocationManager lm = (LocationManager) getBaseContext().getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                Call<ApiUser> call = fcmObj.apiUser(token, userid, location.getLatitude(), location.getLongitude());
                call.enqueue(userCallback);
            }
        }
    }
}
