package org.uiieditt.kelilingaja.lapak;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.ApiMenu;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.util.UserPreferences;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BukaMenuActivity extends AppCompatActivity implements View.OnClickListener,
        DialogInterface.OnClickListener, Callback<ApiMenu> {

    static final int REQUEST_MENU_CAPTURE = 2;

    private EditText namaMenu;
    private EditText hargaMenu;
    private EditText deskripsiMenu;

    private LapakNearby.Lapak lapakObject;

    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buka_menu);

        UserPreferences.initializeInstance(this);

        Bundle data = getIntent().getExtras();
        lapakObject = data.getParcelable("LAPAK");

        namaMenu = (EditText) findViewById(R.id.owner_input_nama_menu);
        hargaMenu = (EditText) findViewById(R.id.owner_input_harga_menu);
        deskripsiMenu = (EditText) findViewById(R.id.owner_input_deskripsi_menu);

        Button submit = (Button) findViewById(R.id.menu_submit);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        boolean cameraGrant = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (cameraGrant) {
            Intent takePictureIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_MENU_CAPTURE);
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_MENU_CAPTURE && resultCode == RESULT_OK) {

            // get image result
            //Bundle extras = data.getExtras();
            //Bitmap imageBitmap = (Bitmap) extras.get("data");

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            final String picturePath = cursor.getString(columnIndex);
            cursor.close();

            String nama = namaMenu.getText().toString();
            String harga = hargaMenu.getText().toString();
            String deskripsi = deskripsiMenu.getText().toString();

            if (nama.equals("")) {
                ValidationDialog();
            }
            if (harga.equals("")) {
                ValidationDialog();
            }
            if (deskripsi.equals("")) {
                ValidationDialog();
            }

            if (!nama.equals("") && !harga.equals("") && !deskripsi.equals("")) {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Apps.API_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(Apps.AbstractConnection())
                        .build();

                final LapakRetrofit lapakRetrofit = retrofit.create(LapakRetrofit.class);

                Log.e("VAR", lapakObject.getLapakid());
                Log.e("VAR", nama);
                Log.e("VAR", harga);
                Log.e("VAR", deskripsi);

                Call<ApiMenu> call = lapakRetrofit.addMenuLapak(lapakObject.getLapakid(),
                        UserPreferences.getInstance().getValue("U_ID"),
                        nama, harga, deskripsi);

                progress = ProgressDialog.show(this, "Menyimpan Data", "Harap tunggu proses penyimpanan data", true);

                call.enqueue(new Callback<ApiMenu>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiMenu> call, @NonNull Response<ApiMenu> response) {

                        Log.v("POST", new Gson().toJson(response.body()));

                        ApiMenu.DataTemplate template = response.body().getData();
                        ApiMenu.Menu menuObj = template.getMenu();

                        try {
                            File filePicture = new File(picturePath);
                            filePicture = new Compressor(getBaseContext())
                                    .setMaxWidth(640)
                                    .setMaxHeight(480)
                                    .setQuality(75)
                                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                    .setDestinationDirectoryPath(getFilesDir().getAbsolutePath())
                                    .compressToFile(filePicture);

                            UpoadMenuFile(filePicture, lapakRetrofit, menuObj.getMenuid());

                        } catch (IOException | NullPointerException ioe) {
                            ioe.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiMenu> call, @NonNull Throwable t) {
                        Log.v("POST", "POST GAGAL");
                        Log.v("POST", t.getMessage());
                        FailureDialog();
                    }
                });
            }
        }
    }

    public void ValidationDialog() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_black_48dp)
                .setTitle(getString(R.string.validation))
                .setMessage("Mohon untuk lengkapi data menu terlebih dahulu.")
                .setPositiveButton(getString(R.string.lanjutkan), this)
                .show();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    @Override
    public void onResponse(@NonNull Call<ApiMenu> call, @NonNull Response<ApiMenu> response) {
        Log.v("POST", new Gson().toJson(response.body()));
        finish();
    }

    @Override
    public void onFailure(@NonNull Call<ApiMenu> call, @NonNull Throwable t) {
        Log.v("POST", t.getMessage());
        FailureDialog();
    }

    public void FailureDialog() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_black_48dp)
                .setTitle(getString(R.string.gagal))
                .setMessage("Input data gagal. Mohon mencoba beberapa saat lagi.")
                .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void UpoadMenuFile(final File filePicture, LapakRetrofit lapakAdd, String menuId) {

        Log.e("MENUID", menuId);

        String fileName = namaMenu.getText().toString().replace(" ", "_") + ".png";

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), filePicture);
        MultipartBody.Part body = MultipartBody.Part.createFormData("fileimage", menuId + "_" + fileName, requestFile);

        Call<ApiMenu> uploadGambarMenu = lapakAdd.addGambarMenu(menuId, body);

        uploadGambarMenu.enqueue(new Callback<ApiMenu>() {
            @Override
            public void onResponse(@NonNull Call<ApiMenu> call, @NonNull Response<ApiMenu> response) {
                Log.v("POST", new Gson().toJson(response.body()));
                Log.v("POST", filePicture.getName());
                if (deleteFile(filePicture.getName())) {
                    progress.dismiss();
                    finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiMenu> call, @NonNull Throwable t) {
                if (deleteFile(filePicture.getName())) {
                    progress.dismiss();
                    FailureDialog();
                }
            }
        });
    }
}
