package org.uiieditt.kelilingaja.lapak;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.LapakNearby;

import java.util.List;

public class LapakAdapter extends RecyclerView.Adapter<LapakAdapter.MyViewHolder> {

    private List<LapakNearby.Lapak> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView namaLapak, jarakLapak, tutupLapak;

        public ImageView image;

        public LapakNearby.Lapak lapak;

        public MyViewHolder(View view) {
            super(view);
            namaLapak = (TextView) view.findViewById(R.id.nama_lapak_rekomendasi);
            jarakLapak = (TextView) view.findViewById(R.id.jarak_lapak_rekomendasi);
            tutupLapak = (TextView) view.findViewById(R.id.tutup_lapak_rekomendasi);

            image = (ImageView) view.findViewById(R.id.list_row_recommendation_image);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), LapakMenuActivity.class);

            Activity act = (Activity) view.getContext();
            intent.putExtra("LAPAK", lapak);

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    act, new Pair<>(view, act.getString(R.string.transition_name_image))
            );

            ActivityCompat.startActivity(act, intent, options.toBundle());
        }
    }

    public LapakAdapter(List<LapakNearby.Lapak> moviesList) {
        this.moviesList = moviesList;
    }

    public void setMoviesList(List<LapakNearby.Lapak> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_recomendation, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LapakNearby.Lapak movie = moviesList.get(position);

        holder.lapak = movie;

        holder.namaLapak.setText(movie.getNamalapak());
        holder.jarakLapak.setText(movie.getDistancem());
        holder.tutupLapak.setText(movie.getTutup());

        Context context = holder.image.getContext();

        Picasso.with(context).load(movie.getImageurl()).fit().centerCrop().into(holder.image);
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
