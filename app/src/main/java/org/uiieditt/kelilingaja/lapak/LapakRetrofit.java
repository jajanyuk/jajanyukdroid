package org.uiieditt.kelilingaja.lapak;

import org.uiieditt.kelilingaja.api.data.ApiLapak;
import org.uiieditt.kelilingaja.api.data.ApiMenu;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.api.data.ListKategori;
import org.uiieditt.kelilingaja.api.data.ListLabel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface LapakRetrofit {

    @FormUrlEncoded
    @POST("lapak/add")
    Call<ApiLapak> addLapak(@Field("lat") Double lat,
                            @Field("lng") Double lng,
                            @Field("userid") String userid,
                            @Field("token") String token,
                            @Field("namalapak") String namalapak,
                            @Field("kategoriid") String kategoriid,
                            @Field("labelid") String labelid,
                            @Field("buka") String buka,
                            @Field("tutup") String tutup);

    @FormUrlEncoded
    @POST("lapak/reopen/{lapakid}")
    Call<ApiLapak> bukaUlangLapak(@Path("lapakid") String lapakid,
                                  @Field("lat") Double lat,
                                  @Field("lng") Double lng,
                                  @Field("userid") String userid,
                                  @Field("token") String token,
                                  @Field("buka") String buka,
                                  @Field("tutup") String tutup);

    @Multipart
    @POST("lapak/file/add/{lapakid}")
    Call<ApiLapak> addGambarLapak(@Path("lapakid") String lapakid,
                                  @Part MultipartBody.Part fileimage);


    @FormUrlEncoded
    @POST("menu/add")
    Call<ApiMenu> addMenuLapak(@Field("lapakid") String lapakid,
                               @Field("userid") String userid,
                               @Field("nama") String nama,
                               @Field("harga") String harga,
                               @Field("deskripsi") String deskripsi);

    @Multipart
    @POST("menu/file/add/{menuid}")
    Call<ApiMenu> addGambarMenu(@Path("menuid") String menuid,
                                @Part MultipartBody.Part fileimage);

    @GET("label")
    Call<ListLabel> listLabel();

    @GET("kategori")
    Call<ListKategori> listKategori();

    @GET("lapak/owner/{lat}/{lng}/{pemilikid}")
    Call<LapakNearby> ownerLapak(@Path("lat") Double latitude, @Path("lng") Double longitude,
                                 @Path("pemilikid") String pemilikid);
}
