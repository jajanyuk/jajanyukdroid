package org.uiieditt.kelilingaja.lapak;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.api.data.MenuLapak;
import org.uiieditt.kelilingaja.menu.MenuRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LapakMenuActivity extends AppCompatActivity implements Callback<MenuLapak> {

    private LapakMenuAdapter mAdapter;

    private LapakNearby.Lapak lapakObject;

    private TextView empty;
    private ProgressBar loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lapak_menu);

        empty = (TextView) findViewById(R.id.owner_menu_recycler_empty);
        loading = (ProgressBar) findViewById(R.id.owner_menu_recycler_loading);

        Bundle data = getIntent().getExtras();
        lapakObject = data.getParcelable("LAPAK");

        ActionBar toolbar = getSupportActionBar();
        toolbar.setTitle(lapakObject.getNamalapak());

        RecyclerView menuRecyclerView = (RecyclerView) findViewById(R.id.owner_menu_recycler_view);

        mAdapter = new LapakMenuAdapter(new ArrayList<MenuLapak.Menu>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        menuRecyclerView.setLayoutManager(mLayoutManager);
        menuRecyclerView.setItemAnimator(new DefaultItemAnimator());
        menuRecyclerView.setAdapter(mAdapter);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        MenuRetrofit nearbyAdapter = retrofit.create(MenuRetrofit.class);

        Call<MenuLapak> call = nearbyAdapter.menuLapak(Integer.parseInt(lapakObject.getLapakid()));
        call.enqueue(this);
    }

    @Override
    public void onResponse(@NonNull Call<MenuLapak> call, @NonNull Response<MenuLapak> response) {
        loading.setVisibility(View.GONE);
        MenuLapak.DataTemplate data = response.body().getData();
        reload(data.getMenu());
    }

    @Override
    public void onFailure(@NonNull Call<MenuLapak> call, @NonNull Throwable t) {
        loading.setVisibility(View.GONE);
        empty.setVisibility(View.VISIBLE);
    }

    public void reload(List<MenuLapak.Menu> lapakData) {
        if (lapakData.size() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
            mAdapter.setMenuList(lapakData);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_lapak_add_menu:
                Intent buka = new Intent(this, BukaMenuActivity.class);
                buka.putExtra("LAPAK", lapakObject);

                ActivityOptionsCompat optionBuka = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        this, new Pair<>(getCurrentFocus(), this.getString(R.string.transition_name_menu))
                );
                ActivityCompat.startActivity(this, buka, optionBuka.toBundle());
                break;
            case R.id.menu_lapak_reopen:
                Intent bukaUlang = new Intent(this, BukaUlangLapakActivity.class);
                bukaUlang.putExtra("LAPAK", lapakObject);

                ActivityOptionsCompat optionBukaUlang = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        this, new Pair<>(getCurrentFocus(), this.getString(R.string.transition_name_menu))
                );
                ActivityCompat.startActivity(this, bukaUlang, optionBukaUlang.toBundle());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        MenuRetrofit nearbyAdapter = retrofit.create(MenuRetrofit.class);

        Call<MenuLapak> call = nearbyAdapter.menuLapak(Integer.parseInt(lapakObject.getLapakid()));
        call.enqueue(this);
    }
}
