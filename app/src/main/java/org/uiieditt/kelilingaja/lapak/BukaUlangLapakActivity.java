package org.uiieditt.kelilingaja.lapak;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.ApiLapak;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.util.UserPreferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BukaUlangLapakActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener, View.OnClickListener, GoogleMap.OnMapClickListener {

    private TextView latText;
    private TextView lngText;

    private Spinner bukaDalam;
    private Spinner tutupDalam;

    private ProgressDialog progress;

    private MapView mapView;
    private GoogleMap mMap;
    private Marker customLocation;

    private LapakNearby.Lapak lapakObject;

    @Override
    protected void onStart() {
        super.onStart();
        UserPreferences.initializeInstance(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buka_ulang_lapak);

        Bundle data = getIntent().getExtras();
        lapakObject = data.getParcelable("LAPAK");

        bukaDalam = (Spinner) findViewById(R.id.buka_dalam);
        bukaDalam.setPrompt("Grobak Buka Pada");
        bukaDalam.setAdapter(this.InitSelectBukaDalam());

        tutupDalam = (Spinner) findViewById(R.id.tutup_dalam);
        tutupDalam.setPrompt("Grobak Tutup Pada");
        tutupDalam.setAdapter(this.InitSelectTutupDalam());

        latText = (TextView) findViewById(R.id.lat_init);
        lngText = (TextView) findViewById(R.id.lng_init);

        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) findViewById(R.id.lapak_map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        Button bukaUlang = (Button) findViewById(R.id.btn_buka_ulang_lapak);
        bukaUlang.setOnClickListener(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng here = new LatLng(location.getLatitude(), location.getLongitude());
        customLocation = mMap.addMarker(new MarkerOptions().position(here).title("Lokasi Kustom"));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(here, 16);
        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onClick(View v) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        // Create an instance of our GitHub API interface.
        final LapakRetrofit lapakAdd = retrofit.create(LapakRetrofit.class);

        // Create a call instance for looking up Retrofit contributors.
        Double lat = Double.parseDouble(latText.getText().toString());
        Double lng = Double.parseDouble(lngText.getText().toString());
        String token = UserPreferences.getInstance().getValue("TOKEN");

        progress = ProgressDialog.show(this, "Menyimpan Data", "Harap tunggu proses penyimpanan data", true);

        Call<ApiLapak> uploadLapak = lapakAdd.bukaUlangLapak(lapakObject.getLapakid(),
                lat, lng,
                UserPreferences.getInstance().getValue("U_ID"),
                token,
                bukaDalam.getSelectedItem().toString(),
                tutupDalam.getSelectedItem().toString());
        uploadLapak.enqueue(lapakCall);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        double latitude = latLng.latitude;
        double longitude = latLng.longitude;

        latText.setText(String.valueOf(latitude));
        lngText.setText(String.valueOf(longitude));

        if (customLocation != null) {
            customLocation.remove();
        }
        LatLng here = new LatLng(latitude, longitude);
        customLocation = mMap.addMarker(new MarkerOptions().position(here).title("Lokasi Kustom"));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_json));
        mMap = googleMap;
        //bug resume it for load maps
        mapView.onResume();
        
        boolean fineLocGrant = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {
            mMap.setMyLocationEnabled(true);
            mMap.setOnMapClickListener(this);

            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            List<String> providers = locationManager.getProviders(true);
            Location bestLocation = null;
            for (String provider : providers) {
                Location l = locationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                }
            }
            if (bestLocation != null) {
                double latitude = bestLocation.getLatitude();
                double longitude = bestLocation.getLongitude();

                //todo best location
            }
        }

        double latitudeDefgault = Double.parseDouble(lapakObject.getLat());
        double longitudeDefault = Double.parseDouble(lapakObject.getLng());

        latText.setText(String.valueOf(latitudeDefgault));
        lngText.setText(String.valueOf(longitudeDefault));

        LatLng latLngDefault = new LatLng(latitudeDefgault, longitudeDefault);
        CameraUpdate cameraUpdateDefault = CameraUpdateFactory.newLatLngZoom(latLngDefault, 16);
        LatLng here = new LatLng(latitudeDefgault, longitudeDefault);
        customLocation = mMap.addMarker(new MarkerOptions().position(here).title("Lokasi Kustom"));
        mMap.animateCamera(cameraUpdateDefault);

    }


    private ArrayAdapter<String> InitSelectBukaDalam() {
        String[] data = new String[]{
                "Sekarang",
                "15 Menit Lagi",
                "30 Menit Lagi",
                "1 Jam Lagi",
                "2 Jam Lagi",
                "3 Jam Lagi"
        };
        return new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, data);
    }

    private ArrayAdapter<String> InitSelectTutupDalam() {
        String[] data = new String[]{
                "2 Jam Setelah Buka",
                "3 Jam Setelah Buka",
                "4 Jam Setelah Buka",
                "5 Jam Setelah Buka",
                "6 Jam Setelah Buka",
                "7 Jam Setelah Buka",
                "8 Jam Setelah Buka",
                "9 Jam Setelah Buka",
                "10 Jam Setelah Buka",
                "11 Jam Setelah Buka",
                "12 Jam Setelah Buka",
                "13 Jam Setelah Buka",
                "14 Jam Setelah Buka",
                "15 Jam Setelah Buka"
        };
        return new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, data);
    }

    private Callback<ApiLapak> lapakCall = new Callback<ApiLapak>() {
        @Override
        public void onResponse(@NonNull Call<ApiLapak> call, @NonNull Response<ApiLapak> response) {

            Log.v("POST", new Gson().toJson(response.body()));

            ApiLapak.DataTemplate template = response.body().getData();
            ApiLapak.Lapak lp = template.getLapak();

            progress.dismiss();
            finish();
        }

        @Override
        public void onFailure(@NonNull Call<ApiLapak> call, @NonNull Throwable t) {
            Log.v("POST", "POonFailureST GAGAL");
            Log.v("POST", t.getMessage());
            progress.dismiss();
            FailureDialog();
        }
    };

    public void FailureDialog() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_black_48dp)
                .setTitle(getString(R.string.gagal))
                .setMessage("Input data gagal. Mohon mencoba beberapa saat lagi.")
                .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
