package org.uiieditt.kelilingaja.lapak;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.LapakNearby;
import org.uiieditt.kelilingaja.beranda.sections.recomendation.RecommendationAdapter;
import org.uiieditt.kelilingaja.util.UserPreferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LapakActivity extends AppCompatActivity implements Callback<LapakNearby> {

    private TextView empty;
    private ProgressBar loading;

    private LapakAdapter mAdapter;

    @Override
    public void onStart() {
        super.onStart();

        UserPreferences.initializeInstance(this);

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {
            // Create a very simple REST adapter which points the GitHub API.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Apps.AbstractConnection())
                    .build();

            // Create an instance of our GitHub API interface.
            LapakRetrofit lapakRetrofit = retrofit.create(LapakRetrofit.class);

            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                // Create a call instance for looking up Retrofit contributors.
                Call<LapakNearby> call = lapakRetrofit.ownerLapak(latitude, longitude, UserPreferences.getInstance().getValue("U_ID"));
                call.enqueue(this);
            }

        } else {
            //feature not allowed
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            }, 1);
        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lapak);

        empty = (TextView) findViewById(R.id.owner_recycler_empty);
        loading = (ProgressBar) findViewById(R.id.owner_recycler_loading);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.owner_recycler_view);

        mAdapter = new LapakAdapter(new ArrayList<LapakNearby.Lapak>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lapak, menu);

        // Associate searchable configuration with the SearchView
        /*
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        */
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*
            case R.id.search:
                break;
            */
            case R.id.menu_lapak_add:
                Intent buka = new Intent(this, BukaLapakActivity.class);
                ActivityOptionsCompat optionBuka = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        this, new Pair<>(getCurrentFocus(), this.getString(R.string.transition_name_image))
                );
                ActivityCompat.startActivity(this, buka, optionBuka.toBundle());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(@NonNull Call<LapakNearby> call, @NonNull Response<LapakNearby> response) {
        loading.setVisibility(View.GONE);
        LapakNearby.DataTemplate data = response.body().getData();
        reload(data.getLapak());
    }

    @Override
    public void onFailure(@NonNull Call<LapakNearby> call, @NonNull Throwable t) {
        loading.setVisibility(View.GONE);
        empty.setVisibility(View.VISIBLE);
    }

    public void reload(List<LapakNearby.Lapak> lapakData) {
        if (lapakData.size() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
            mAdapter.setMoviesList(lapakData);
            mAdapter.notifyDataSetChanged();
        }
    }
}