package org.uiieditt.kelilingaja.lapak;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.uiieditt.kelilingaja.api.data.ListLabel;

import java.util.ArrayList;

public class LabelAdapter extends ArrayAdapter<ArrayList<ListLabel.Label>> implements Filterable {

    private ArrayList<ListLabel.Label> labelList;
    private ArrayList<ListLabel.Label> originalList;
    private LabelFilter mFilter;

    public LabelAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<ListLabel.Label> labelList) {
        super(context, resource);
        this.labelList = labelList;
        originalList = new ArrayList<>(this.labelList);
    }

    @Override
    public int getCount() {
        return labelList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }

        TextView label = (TextView) convertView.findViewById(android.R.id.text1);
        label.setText(labelList.get(position).getLabel());

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new LabelFilter();
        }
        return mFilter;
    }

    private class LabelFilter extends Filter {

        private Object lock;

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (originalList == null) {
                synchronized (lock) {
                    originalList = new ArrayList<>(labelList);
                }
            }

            if (prefix == null || prefix.length() == 0) {
                synchronized (lock) {
                    ArrayList<ListLabel.Label> list = new ArrayList<>(originalList);
                    results.values = list;
                    results.count = list.size();
                }
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                ArrayList<ListLabel.Label> values = originalList;
                int count = values.size();

                ArrayList<ListLabel.Label> newValues = new ArrayList<>(count);

                for (int i = 0; i < count; i++) {
                    ListLabel.Label item = values.get(i);
                    if (item.getLabel().toLowerCase().contains(prefixString)) {
                        newValues.add(item);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                labelList = (ArrayList<ListLabel.Label>) results.values;
            } else {
                labelList = new ArrayList<>();
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
