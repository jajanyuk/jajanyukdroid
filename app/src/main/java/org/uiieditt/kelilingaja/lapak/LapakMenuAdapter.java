package org.uiieditt.kelilingaja.lapak;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.MenuLapak;

import java.util.List;

public class LapakMenuAdapter extends RecyclerView.Adapter<LapakMenuAdapter.MyViewHolder> {

    private List<MenuLapak.Menu> menuList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_menu_owner, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MenuLapak.Menu menu = menuList.get(position);

        holder.menu = menu;

        holder.name.setText(menu.getNama());
        holder.harga.setText(menu.getHarga());
        holder.terjual.setText(menu.getDeskripsi());

        Context context = holder.image.getContext();

        Picasso.with(context).load(menu.getImageurl()).fit().centerCrop().into(holder.image);
    }

    public LapakMenuAdapter(List<MenuLapak.Menu> menuList) {
        this.menuList = menuList;
    }

    public void setMenuList(List<MenuLapak.Menu> menuList) {
        this.menuList = menuList;
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,
            View.OnClickListener, NumberPicker.OnValueChangeListener {

        public TextView name, harga, terjual;

        public ImageView image;

        public MenuLapak.Menu menu;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.owner_list_row_menu_name);
            harga = (TextView) view.findViewById(R.id.owner_list_row_menu_harga);
            terjual = (TextView) view.findViewById(R.id.owner_list_row_menu_terjual);

            image = (ImageView) view.findViewById(R.id.owner_list_row_menu_image);

            view.setOnCreateContextMenuListener(this);
            view.setOnClickListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0, v.getId(), 0, "Call");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "SMS");
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        }
    }
}
