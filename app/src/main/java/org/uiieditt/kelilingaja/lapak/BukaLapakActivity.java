package org.uiieditt.kelilingaja.lapak;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.R;
import org.uiieditt.kelilingaja.api.data.ApiLapak;
import org.uiieditt.kelilingaja.api.data.ListKategori;
import org.uiieditt.kelilingaja.api.data.ListLabel;
import org.uiieditt.kelilingaja.util.UserPreferences;

import java.io.File;
import java.io.IOException;
import java.util.List;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BukaLapakActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener, View.OnClickListener, GoogleMap.OnMapClickListener {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private EditText namaLapak;

    private TextView latText;
    private TextView lngText;

    private MapView mapView;
    private GoogleMap mMap;

    private Marker customLocation;
    private MultiAutoCompleteTextView labelText;
    private Spinner kategori;

    private ArrayAdapter<String> labelKategori;
    private ArrayAdapter<String> labelLabel;

    private String[] stringIdKategori;
    private String[] stringIdLabel;

    private Spinner bukaDalam;
    private Spinner tutupDalam;

    private ProgressDialog progress;

    @Override
    protected void onStart() {
        super.onStart();
        labelKategori = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line);
        labelLabel = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line);

        UserPreferences.initializeInstance(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buka_lapak);

        kategori = (Spinner) findViewById(R.id.kategori_lapak);
        kategori.setPrompt("Pilih Kategori");

        bukaDalam = (Spinner) findViewById(R.id.buka_dalam);
        bukaDalam.setPrompt("Grobak Buka Pada");
        bukaDalam.setAdapter(this.InitSelectBukaDalam());

        tutupDalam = (Spinner) findViewById(R.id.tutup_dalam);
        tutupDalam.setPrompt("Grobak Tutup Pada");
        tutupDalam.setAdapter(this.InitSelectTutupDalam());

        latText = (TextView) findViewById(R.id.lat_init);
        lngText = (TextView) findViewById(R.id.lng_init);

        labelText = (MultiAutoCompleteTextView) findViewById(R.id.buka_lapak_label_input);
        labelText.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        namaLapak = (EditText) findViewById(R.id.nama_lapak);
        Button selanjutnya = (Button) findViewById(R.id.buka_lapak_selanjutnya);
        selanjutnya.setOnClickListener(this);

        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) findViewById(R.id.lapak_map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        // Create a very simple REST adapter which points the GitHub API.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(Apps.AbstractConnection())
                .build();

        // Create an instance of our GitHub API interface.
        LapakRetrofit listLabel = retrofit.create(LapakRetrofit.class);

        // Create a call instance for looking up Retrofit contributors.
        Call<ListKategori> kategoriCall = listLabel.listKategori();
        kategoriCall.enqueue(new Callback<ListKategori>() {
            @Override
            public void onResponse(@NonNull Call<ListKategori> call, @NonNull Response<ListKategori> response) {
                ListKategori.DataTemplate data = response.body().getData();

                String[] stringKategori = new String[data.getKategori().size()];
                stringIdKategori = new String[data.getKategori().size()];

                for (int i = 0; i < data.getKategori().size(); i++) {
                    ListKategori.Kategori l = data.getKategori().get(i);
                    stringKategori[i] = l.getKategori();
                    stringIdKategori[i] = l.getKategoriid();
                }

                labelKategori = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, stringKategori);
                kategori.setAdapter(labelKategori);
            }

            @Override
            public void onFailure(@NonNull Call<ListKategori> call, @NonNull Throwable t) {

            }
        });

        Call<ListLabel> labelCall = listLabel.listLabel();
        labelCall.enqueue(new Callback<ListLabel>() {
            @Override
            public void onResponse(@NonNull Call<ListLabel> call, @NonNull Response<ListLabel> response) {
                ListLabel.DataTemplate data = response.body().getData();

                String[] stringLabel = new String[data.getLabel().size()];
                stringIdLabel = new String[data.getLabel().size()];

                for (int i = 0; i < data.getLabel().size(); i++) {
                    ListLabel.Label l = data.getLabel().get(i);
                    stringLabel[i] = l.getLabel();
                    stringIdLabel[i] = l.getLabelid();
                }

                labelLabel = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, stringLabel);
                labelText.setAdapter(labelLabel);
            }

            @Override
            public void onFailure(@NonNull Call<ListLabel> call, @NonNull Throwable t) {

            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_json));
        mMap = googleMap;
        mapView.onResume();

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {
            mMap.setMyLocationEnabled(true);
            mMap.setOnMapClickListener(this);

            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            //TODO: begin fix for searching best location found
            List<String> providers = locationManager.getProviders(true);
            Location bestLocation = null;
            for (String provider : providers) {
                Location l = locationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                }
            }
            if (bestLocation != null) {
                double latitude = bestLocation.getLatitude();
                double longitude = bestLocation.getLongitude();

                latText.setText(String.valueOf(latitude));
                lngText.setText(String.valueOf(longitude));

                LatLng here = new LatLng(latitude, longitude);
                customLocation = mMap.addMarker(new MarkerOptions().position(here).title("Lokasi Kustom"));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(here, 16);
                mMap.animateCamera(cameraUpdate);
            } else {
                double latitudeDefgault = -6.914744;
                double longitudeDefault = 107.609810;

                LatLng latLngDefault = new LatLng(latitudeDefgault, longitudeDefault);
                CameraUpdate cameraUpdateDefault = CameraUpdateFactory.newLatLngZoom(latLngDefault, 12);
                mMap.animateCamera(cameraUpdateDefault);
            }
            //TODO: end of fix for searching best location found
        }
    }

    @Override
    public void onClick(View v) {
        boolean cameraGrant = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (cameraGrant) {
            Intent takePictureIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 1);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            // get image result
            //Bundle extras = data.getExtras();
            //Bitmap imageBitmap = (Bitmap) extras.get("data");

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            final String picturePath = cursor.getString(columnIndex);
            cursor.close();

            // Create a very simple REST adapter which points the GitHub API.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Apps.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(Apps.AbstractConnection())
                    .build();

            // Create an instance of our GitHub API interface.
            final LapakRetrofit lapakAdd = retrofit.create(LapakRetrofit.class);

            // Create a call instance for looking up Retrofit contributors.
            Double lat = Double.parseDouble(latText.getText().toString());
            Double lng = Double.parseDouble(lngText.getText().toString());
            String token = UserPreferences.getInstance().getValue("TOKEN");

            Call<ApiLapak> uploadLapak = lapakAdd.addLapak(lat, lng, UserPreferences.getInstance().getValue("U_ID"), token,
                    namaLapak.getText().toString(),
                    stringIdKategori[kategori.getSelectedItemPosition()],
                    labelText.getText().toString(),
                    bukaDalam.getSelectedItem().toString(),
                    tutupDalam.getSelectedItem().toString());

            progress = ProgressDialog.show(this, "Menyimpan Data", "Harap tunggu proses penyimpanan data", true);

            uploadLapak.enqueue(new Callback<ApiLapak>() {
                @Override
                public void onResponse(@NonNull Call<ApiLapak> call, @NonNull Response<ApiLapak> response) {

                    Log.v("POST", new Gson().toJson(response.body()));

                    ApiLapak.DataTemplate template = response.body().getData();
                    ApiLapak.Lapak lp = template.getLapak();

                    try {
                        File filePicture = new File(picturePath);
                        filePicture = new Compressor(getBaseContext())
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(75)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setDestinationDirectoryPath(getFilesDir().getAbsolutePath())
                                .compressToFile(filePicture);

                        UpoadLapakFile(filePicture, lapakAdd, lp.getLapakid());

                    } catch (IOException | NullPointerException ioe) {
                        ioe.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ApiLapak> call, @NonNull Throwable t) {
                    Log.v("POST", "POST GAGAL");
                    Log.v("POST", t.getMessage());
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("POST", "ON RESUME TRIGERED");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else {

                }
                break;
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        double latitude = latLng.latitude;
        double longitude = latLng.longitude;

        latText.setText(String.valueOf(latitude));
        lngText.setText(String.valueOf(longitude));

        if (customLocation != null) {
            customLocation.remove();
        }
        LatLng here = new LatLng(latitude, longitude);
        customLocation = mMap.addMarker(new MarkerOptions().position(here).title("Lokasi Kustom"));
    }

    private ArrayAdapter<String> InitSelectBukaDalam() {
        String[] data = new String[]{
                "Sekarang",
                "15 Menit Lagi",
                "30 Menit Lagi",
                "1 Jam Lagi",
                "2 Jam Lagi",
                "3 Jam Lagi"
        };
        return new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, data);
    }

    private ArrayAdapter<String> InitSelectTutupDalam() {
        String[] data = new String[]{
                "2 Jam Setelah Buka",
                "3 Jam Setelah Buka",
                "4 Jam Setelah Buka",
                "5 Jam Setelah Buka",
                "6 Jam Setelah Buka",
                "7 Jam Setelah Buka",
                "8 Jam Setelah Buka",
                "9 Jam Setelah Buka",
                "10 Jam Setelah Buka",
                "11 Jam Setelah Buka",
                "12 Jam Setelah Buka",
                "13 Jam Setelah Buka",
                "14 Jam Setelah Buka",
                "15 Jam Setelah Buka"
        };
        return new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, data);
    }

    private void UpoadLapakFile(final File filePicture, LapakRetrofit lapakAdd, String lapakId) {

        Log.e("LAPAKID", lapakId);

        String fileName = namaLapak.getText().toString().replace(" ", "_") + ".png";

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), filePicture);
        MultipartBody.Part body = MultipartBody.Part.createFormData("fileimage", lapakId + "_" + fileName, requestFile);

        Call<ApiLapak> uploadGambarLapak = lapakAdd.addGambarLapak(lapakId, body);

        uploadGambarLapak.enqueue(new Callback<ApiLapak>() {
            @Override
            public void onResponse(@NonNull Call<ApiLapak> call, @NonNull Response<ApiLapak> response) {
                Log.v("POST", new Gson().toJson(response.body()));
                Log.v("POST", filePicture.getName());
                if (deleteFile(filePicture.getName())) {
                    progress.dismiss();
                    finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiLapak> call, @NonNull Throwable t) {
                if (deleteFile(filePicture.getName())) {
                    progress.dismiss();
                    FailureDialog();
                }
            }
        });
    }

    public void FailureDialog() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_black_48dp)
                .setTitle(getString(R.string.gagal))
                .setMessage("Input data gagal. Mohon mencoba beberapa saat lagi.")
                .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
