package org.uiieditt.kelilingaja.service;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.uiieditt.kelilingaja.Apps;
import org.uiieditt.kelilingaja.api.data.ApiUser;
import org.uiieditt.kelilingaja.util.UserPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class IDUserNotification extends FirebaseInstanceIdService implements Callback<ApiUser> {

    private static final String TAG = IDUserNotification.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Apps.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        UserPreferences.initializeInstance(getApplicationContext());

        String userid = UserPreferences.getInstance().getValue("U_ID");

        if (userid == null) {
            userid = "0";
        }

        Log.v(TAG, userid);

        // Create a very simple REST adapter which points the GitHub API.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Apps.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our GitHub API interface.
        UserServiceRetrofit fcmObj = retrofit.create(UserServiceRetrofit.class);

        boolean fineLocGrant = ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocGrant) {
            LocationManager lm = (LocationManager) getBaseContext().getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                Call<ApiUser> call = fcmObj.apiUser(token, userid, location.getLatitude(), location.getLongitude());
                call.enqueue(this);
            }
        }
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        UserPreferences.initializeInstance(getApplicationContext());
        UserPreferences.getInstance().setValue("FCM_REGID", token);
    }

    @Override
    public void onResponse(@NonNull Call<ApiUser> call, @NonNull Response<ApiUser> response) {
        Log.v(TAG, response.body().toString());
        Log.v(TAG, "RESPONSE SUCCESS");
    }

    @Override
    public void onFailure(@NonNull Call<ApiUser> call, @NonNull Throwable t) {
        Log.v(TAG, t.getMessage().toString());
        Log.v(TAG, "RESPONSE FAILED");
    }
}
