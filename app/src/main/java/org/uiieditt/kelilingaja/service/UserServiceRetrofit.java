package org.uiieditt.kelilingaja.service;

import org.uiieditt.kelilingaja.api.data.ApiUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserServiceRetrofit {

    @FormUrlEncoded
    @POST("user/input")
    Call<ApiUser> apiUser(@Field("fcmid") String fcmid,
                          @Field("userid") String userid,
                          @Field("lat") Double lat,
                          @Field("lng") Double lng);

}
