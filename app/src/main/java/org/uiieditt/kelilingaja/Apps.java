package org.uiieditt.kelilingaja;

import android.app.Application;

import com.evernote.android.job.JobManager;

import org.uiieditt.kelilingaja.job.JajanyukJobCreator;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class Apps extends Application {

    public static final String API_URL = "http://www.kelilingaja.com/";
    public static final String CREDENTIAL_URL = "http://floors.cf/";
    public static final String LOGIN_URL = "http://floors.cf/resume?sso=b57b22e6deed7ce29d6e08e096ea3180ad13d005";

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";

    // The minimum distance to change Updates in meters
    public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    public static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    @Override
    public void onCreate() {
        super.onCreate();

        JajanyukJobCreator jobs = new JajanyukJobCreator();
        JobManager.create(this).addJobCreator(jobs);
    }

    public static OkHttpClient AbstractConnection() {
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
    }
}