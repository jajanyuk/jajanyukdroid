package org.uiieditt.kelilingaja.support;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import org.uiieditt.kelilingaja.R;

public class FaqActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_activity_faq);

        WebView myWebView = (WebView) findViewById(R.id.faq_kelilingaja);
        myWebView.loadUrl("http://www.kelilingaja.com/faq");
    }
}