package org.uiieditt.kelilingaja.support;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import org.uiieditt.kelilingaja.R;

public class ContactJajanyukActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_activity_contact_jajanyuk);

        WebView myWebView = (WebView) findViewById(R.id.about_kelilingaja);
        myWebView.loadUrl("http://www.kelilingaja.com/contacts");
    }
}
