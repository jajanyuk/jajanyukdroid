package org.uiieditt.kelilingaja.saldo;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import org.uiieditt.kelilingaja.R;

public class SaldoActivity extends AppCompatActivity implements View.OnClickListener {

    public Button topupSaldo;
    public Button withdrawnSaldo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saldo);

        topupSaldo = (Button) findViewById(R.id.saldo_topup);
        withdrawnSaldo = (Button) findViewById(R.id.saldo_withdrawn);

        topupSaldo.setOnClickListener(this);
        withdrawnSaldo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saldo_topup:
                Intent add = new Intent(view.getContext(), SaldoAddActivity.class);

                Activity actAdd = (Activity) view.getContext();
                ActivityOptionsCompat optionAdd = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        actAdd, new Pair<>(view, actAdd.getString(R.string.transition_name_image))
                );

                ActivityCompat.startActivity(actAdd, add, optionAdd.toBundle());
                break;
            case R.id.saldo_withdrawn:
                Intent withdrawn = new Intent(view.getContext(), SaldoWithdrawnActivity.class);

                Activity actWithdrawn = (Activity) view.getContext();
                ActivityOptionsCompat optionWithdrawn = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        actWithdrawn, new Pair<>(view, actWithdrawn.getString(R.string.transition_name_image))
                );

                ActivityCompat.startActivity(actWithdrawn, withdrawn, optionWithdrawn.toBundle());
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_saldo, menu);

        /*
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        */
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_application_terms_service) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
