package org.uiieditt.kelilingaja.api.data;

import android.os.Parcel;
import android.os.Parcelable;

import org.uiieditt.kelilingaja.api.WireApi;

import java.util.ArrayList;

@WireApi(url = "api/user") //CREDENTIAL
public class ApiUser {

    private double time;

    private String status;

    private DataTemplate data;

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataTemplate getData() {
        return data;
    }

    public void setData(DataTemplate data) {
        this.data = data;
    }

    public class DataTemplate {

        private String token;

        private User user;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }
    }

    public static class User implements Parcelable {

        public String id;
        public String alias;
        public String suffix;
        public String fullname;
        public String prefix;
        public String phonenumber;
        public String firstemail;
        public String secondemail;
        public String birthday;
        public String descriptions;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public String getSuffix() {
            return suffix;
        }

        public void setSuffix(String suffix) {
            this.suffix = suffix;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public String getPhonenumber() {
            return phonenumber;
        }

        public void setPhonenumber(String phonenumber) {
            this.phonenumber = phonenumber;
        }

        public String getFirstemail() {
            return firstemail;
        }

        public void setFirstemail(String firstemail) {
            this.firstemail = firstemail;
        }

        public String getSecondemail() {
            return secondemail;
        }

        public void setSecondemail(String secondemail) {
            this.secondemail = secondemail;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getDescriptions() {
            return descriptions;
        }

        public void setDescriptions(String descriptions) {
            this.descriptions = descriptions;
        }

        protected User(Parcel in) {
            id = in.readString();
            alias = in.readString();
            suffix = in.readString();
            fullname = in.readString();
            prefix = in.readString();
            phonenumber = in.readString();
            firstemail = in.readString();
            secondemail = in.readString();
            birthday = in.readString();
            descriptions = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(alias);
            dest.writeString(suffix);
            dest.writeString(fullname);
            dest.writeString(prefix);
            dest.writeString(phonenumber);
            dest.writeString(firstemail);
            dest.writeString(secondemail);
            dest.writeString(birthday);
            dest.writeString(descriptions);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
            @Override
            public User createFromParcel(Parcel in) {
                return new User(in);
            }

            @Override
            public User[] newArray(int size) {
                return new User[size];
            }
        };
    }
}
