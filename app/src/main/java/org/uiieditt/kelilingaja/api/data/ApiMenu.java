package org.uiieditt.kelilingaja.api.data;

import android.os.Parcel;
import android.os.Parcelable;

public class ApiMenu {

    private double time;

    private String status;

    private DataTemplate data;

    public class DataTemplate {

        private String token;

        private Menu menu;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Menu getMenu() {
            return menu;
        }

        public void setMenu(Menu menu) {
            this.menu = menu;
        }
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataTemplate getData() {
        return data;
    }

    public void setData(DataTemplate data) {
        this.data = data;
    }

    public static class Menu implements Parcelable {

        public String menuid;
        public String lapakid;
        public String created;
        public String modified;
        public String cuid;
        public String muid;
        public String dflag;
        public String nama;
        public String harga;
        public String deskripsi;
        public String imageurl;

        public String getMenuid() {
            return menuid;
        }

        public void setMenuid(String menuid) {
            this.menuid = menuid;
        }

        public String getLapakid() {
            return lapakid;
        }

        public void setLapakid(String lapakid) {
            this.lapakid = lapakid;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getModified() {
            return modified;
        }

        public void setModified(String modified) {
            this.modified = modified;
        }

        public String getCuid() {
            return cuid;
        }

        public void setCuid(String cuid) {
            this.cuid = cuid;
        }

        public String getMuid() {
            return muid;
        }

        public void setMuid(String muid) {
            this.muid = muid;
        }

        public String getDflag() {
            return dflag;
        }

        public void setDflag(String dflag) {
            this.dflag = dflag;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getHarga() {
            return harga;
        }

        public void setHarga(String harga) {
            this.harga = harga;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public String getImageurl() {
            return imageurl;
        }

        public void setImageurl(String imageurl) {
            this.imageurl = imageurl;
        }

        protected Menu(Parcel in) {
            menuid = in.readString();
            lapakid = in.readString();
            created = in.readString();
            modified = in.readString();
            cuid = in.readString();
            muid = in.readString();
            dflag = in.readString();
            nama = in.readString();
            harga = in.readString();
            deskripsi = in.readString();
            imageurl = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(menuid);
            dest.writeString(lapakid);
            dest.writeString(created);
            dest.writeString(modified);
            dest.writeString(cuid);
            dest.writeString(muid);
            dest.writeString(dflag);
            dest.writeString(nama);
            dest.writeString(harga);
            dest.writeString(deskripsi);
            dest.writeString(imageurl);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Menu> CREATOR = new Parcelable.Creator<Menu>() {
            @Override
            public Menu createFromParcel(Parcel in) {
                return new Menu(in);
            }

            @Override
            public Menu[] newArray(int size) {
                return new Menu[size];
            }
        };
    }
}
