package org.uiieditt.kelilingaja.api.data;

import java.util.ArrayList;

public class ListKategori {

    private double time;

    private String status;

    private DataTemplate data;

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataTemplate getData() {
        return data;
    }

    public void setData(DataTemplate data) {
        this.data = data;
    }

    public class DataTemplate {

        private String token;

        private ArrayList<Kategori> kategori;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public ArrayList<Kategori> getKategori() {
            return kategori;
        }

        public void setKategori(ArrayList<Kategori> kategori) {
            this.kategori = kategori;
        }
    }

    public class Kategori {

        public String kategoriid;

        public String kategori;

        public String getKategoriid() {
            return kategoriid;
        }

        public void setKategoriid(String kategoriid) {
            this.kategoriid = kategoriid;
        }

        public String getKategori() {
            return kategori;
        }

        public void setKategori(String kategori) {
            this.kategori = kategori;
        }
    }
}
