package org.uiieditt.kelilingaja.api.data;

import android.os.Parcel;
import android.os.Parcelable;

import org.uiieditt.kelilingaja.api.WireApi;

import java.util.ArrayList;

@WireApi(url = "api/login/info") //CREDENTIAL
public class LoginInfo {

    private double time;

    private String status;

    private DataTemplate data;

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataTemplate getData() {
        return data;
    }

    public void setData(DataTemplate data) {
        this.data = data;
    }

    public class DataTemplate {

        private String token;

        private ArrayList<Login> login;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public ArrayList<Login> getLogin() {
            return login;
        }

        public void setLogin(ArrayList<Login> login) {
            this.login = login;
        }
    }

    public static class Login implements Parcelable {

        public String id;
        public String userid;
        public String credentialid;
        public String datein;
        public String requestmethod;
        public String action;
        public String ipaddress;
        public String useragent;
        public String httpstatus;
        public String tokens;

        public Login() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getCredentialid() {
            return credentialid;
        }

        public void setCredentialid(String credentialid) {
            this.credentialid = credentialid;
        }

        public String getDatein() {
            return datein;
        }

        public void setDatein(String datein) {
            this.datein = datein;
        }

        public String getRequestmethod() {
            return requestmethod;
        }

        public void setRequestmethod(String requestmethod) {
            this.requestmethod = requestmethod;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public String getIpaddress() {
            return ipaddress;
        }

        public void setIpaddress(String ipaddress) {
            this.ipaddress = ipaddress;
        }

        public String getUseragent() {
            return useragent;
        }

        public void setUseragent(String useragent) {
            this.useragent = useragent;
        }

        public String getHttpstatus() {
            return httpstatus;
        }

        public void setHttpstatus(String httpstatus) {
            this.httpstatus = httpstatus;
        }

        public String getTokens() {
            return tokens;
        }

        public void setTokens(String tokens) {
            this.tokens = tokens;
        }

        protected Login(Parcel in) {
            id = in.readString();
            userid = in.readString();
            credentialid = in.readString();
            datein = in.readString();
            requestmethod = in.readString();
            action = in.readString();
            ipaddress = in.readString();
            useragent = in.readString();
            httpstatus = in.readString();
            tokens = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(userid);
            dest.writeString(credentialid);
            dest.writeString(datein);
            dest.writeString(requestmethod);
            dest.writeString(action);
            dest.writeString(ipaddress);
            dest.writeString(useragent);
            dest.writeString(httpstatus);
            dest.writeString(tokens);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Login> CREATOR = new Parcelable.Creator<Login>() {
            @Override
            public Login createFromParcel(Parcel in) {
                return new Login(in);
            }

            @Override
            public Login[] newArray(int size) {
                return new Login[size];
            }
        };
    }
}
