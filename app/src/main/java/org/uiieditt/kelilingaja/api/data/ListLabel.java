package org.uiieditt.kelilingaja.api.data;

import org.uiieditt.kelilingaja.api.WireApi;

import java.util.ArrayList;

@WireApi(url = "label")
public class ListLabel {

    private double time;

    private String status;

    private DataTemplate data;

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataTemplate getData() {
        return data;
    }

    public void setData(DataTemplate data) {
        this.data = data;
    }

    public class DataTemplate {

        private String token;

        private ArrayList<Label> label;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public ArrayList<Label> getLabel() {
            return label;
        }

        public void setLabel(ArrayList<Label> label) {
            this.label = label;
        }
    }

    public class Label {

        public String labelid;
        public String label;

        public String getLabelid() {
            return labelid;
        }

        public void setLabelid(String labelid) {
            this.labelid = labelid;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }
}
