package org.uiieditt.kelilingaja.api.data;

import android.os.Parcel;
import android.os.Parcelable;

public class ApiLapak {

    private double time;

    private String status;

    private DataTemplate data;

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataTemplate getData() {
        return data;
    }

    public void setData(DataTemplate data) {
        this.data = data;
    }

    public class DataTemplate {

        private String token;

        private Lapak lapak;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Lapak getLapak() {
            return lapak;
        }

        public void setLapak(Lapak lapak) {
            this.lapak = lapak;
        }
    }

    public static class Lapak implements Parcelable {

        private String lapakid;
        private String pemilikid;
        private String kategoriid;
        private String namalapak;

        private String lat;
        private String lng;
        private String buka;
        private String tutup;

        private String imageurl;

        public String getKategoriid() {
            return kategoriid;
        }

        public void setKategoriid(String kategoriid) {
            this.kategoriid = kategoriid;
        }

        public String getLapakid() {
            return lapakid;
        }

        public void setLapakid(String lapakid) {
            this.lapakid = lapakid;
        }

        public String getPemilikid() {
            return pemilikid;
        }

        public void setPemilikid(String pemilikid) {
            this.pemilikid = pemilikid;
        }

        public String getNamalapak() {
            return namalapak;
        }

        public void setNamalapak(String namalapak) {
            this.namalapak = namalapak;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getBuka() {
            return buka;
        }

        public void setBuka(String buka) {
            this.buka = buka;
        }

        public String getTutup() {
            return tutup;
        }

        public void setTutup(String tutup) {
            this.tutup = tutup;
        }

        public String getImageurl() {
            return imageurl;
        }

        public void setImageurl(String imageurl) {
            this.imageurl = imageurl;
        }

        protected Lapak(Parcel in) {
            lapakid = in.readString();
            kategoriid = in.readString();
            pemilikid = in.readString();
            namalapak = in.readString();
            lat = in.readString();
            lng = in.readString();
            buka = in.readString();
            tutup = in.readString();
            imageurl = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(lapakid);
            dest.writeString(kategoriid);
            dest.writeString(pemilikid);
            dest.writeString(namalapak);
            dest.writeString(lat);
            dest.writeString(lng);
            dest.writeString(buka);
            dest.writeString(tutup);
            dest.writeString(imageurl);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Lapak> CREATOR = new Parcelable.Creator<Lapak>() {
            @Override
            public Lapak createFromParcel(Parcel in) {
                return new Lapak(in);
            }

            @Override
            public Lapak[] newArray(int size) {
                return new Lapak[size];
            }
        };
    }
}
